#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Tacka {
	int x, y;
public:
	Tacka(int x = 0, int y = 0) { this->x = x; this->y = y; }
	int getX()const { return x; } void setX(int x) { this->x = x; }
	int getY()const { return y; }
	friend std::ostream& operator<<(std::ostream &o, const Tacka &t) {
		return o << "(" << t.x << ", " << t.y << ")";
	}
	friend bool operator<(const Tacka &t1, const Tacka &t2) { return t1.x < t2.x; }
	friend bool operator==(const Tacka &t1, const Tacka &t2) { return t1.x == t2.x && t1.y == t2.y; }
	double d(const Tacka &t) { return sqrt(pow(x - t.x, 2) + pow(y - t.y, 2)); }
};

class Izlomljena {
	vector<Tacka> tacke;
public:
	Izlomljena& dodaj(Tacka t) {
		tacke.push_back(t); return *this;
	}
	void removeMax() {
		pair<Tacka, Tacka> pair = max();
		auto iter = remove_if(tacke.begin(), tacke.end(), [&pair](Tacka t) {return t == pair.first || t == pair.second; });
		tacke.erase(iter, tacke.end());
	}
	auto begin() { return tacke.begin(); }
	auto end() { return tacke.end(); }
	int count(bool(*predicate)(const Tacka &t)) {
		return count_if(tacke.begin(), tacke.end(), predicate);
	}
	Izlomljena& sort() {
		return sort([](const Tacka &t1, const Tacka &t2)->bool {return t1 < t2; });
	}
	Izlomljena& sort(bool(*predicate)(const Tacka &t1, const Tacka &t2)) {
		std::sort(tacke.begin(), tacke.end(), predicate);
		return *this;
	}
	Izlomljena& rotateY() {
		for_each(tacke.begin(), tacke.end(), [](Tacka &t) {t.setX(t.getX()*-1); });
		return *this;
	}
	pair<Tacka, Tacka> max() {
		pair<Tacka, Tacka> p = make_pair(tacke[0], tacke[1]);
		double d = tacke[0].d(tacke[1]);
		for_each(tacke.begin(), tacke.end(), 
			[this,&d,&p](Tacka t1) {
			for_each(this->tacke.begin(), this->tacke.end(),
				[&](Tacka t2) {
				if (t1.d(t2) > d) {
					d = t1.d(t2);
					p = make_pair(t1, t2);
				}
			});
		});
		return p;
	}
};

void main() {
	Izlomljena i;
	i.dodaj(Tacka());
	i.dodaj(Tacka(-5, 6));
	i.dodaj(Tacka(-4, 3));
	i.dodaj(Tacka(9, 2));
	i.dodaj(Tacka(5, -7));
	
	for (auto e : i) {
		cout << e << endl;
	}

	cout << endl;
	cout << "Broj tacaka kojima je x>3 je: " << i.count([](const Tacka &t)->bool {return t.getX() > 3; }) << endl;
	cout << "Broj tacaka kojima je x>=y je: " << i.count([](const Tacka &t)->bool {return t.getX() >= t.getY(); }) << endl << endl;

	i.sort([](const Tacka &t1, const Tacka &t2)->bool {return t1.getY() < t2.getY(); });
	cout << "Sortirano po Y: " << endl;
	for (auto e : i) {
		cout << e << endl;
	}

	i.rotateY();
	cout << endl << "Rotacija..." << endl;
	for (auto e : i) {
		cout << e << endl;
	}

	cout << endl << "Najudaljenije su: " << endl;
	pair<Tacka, Tacka> pair = i.max();
	cout << pair.first << " i " << pair.second << endl;

	i.removeMax();
	cout << endl << "Uklonjene najudaljenije..." << endl;
	for (auto e : i) {
		cout << e << endl;
	}


	system("pause");
}