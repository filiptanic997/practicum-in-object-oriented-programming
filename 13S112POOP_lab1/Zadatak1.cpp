#include <iostream>
using namespace std;

class Tacka {
	int x, y;
public:
	Tacka(int x = 0, int y = 0) { this->x = x; this->y = y; }
	friend std::ostream& operator<<(std::ostream &o, const Tacka &t) {
		return o << "(" << t.x << ", " << t.y << ")";
	}
};

class Izlomljena {
	Tacka *tacke;
	int n; int kap;
public:
	Izlomljena(int n) { tacke = new Tacka[this->n = n]; }
	~Izlomljena() { delete tacke; }
	bool dodaj(Tacka t) {
		if (kap == n) return false;
		tacke[kap++] = t;
	}
};

void main() {
	
	system("pause");
}