#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <fstream>
#include <string>
#include <regex>
#include <iomanip>

using namespace std;

class Linija;

class Stajaliste {
	int broj;
	string naziv;
	double x, y;
	set<Linija> linije;
public:
	Stajaliste(int broj, string naziv, double x, double y, set<Linija> linije) {
		this->broj = broj;
		this->naziv = naziv;
		this->x = x;
		this->y = y;
		this->linije = linije;
	}
	string getNaziv()const { return naziv; }
	friend bool operator<(const Stajaliste &s1, const Stajaliste &s2) { return s1.broj < s2.broj; }
	friend class Linija;
	friend ostream& operator<<(ostream &ot, const Stajaliste &s);
};

class Linija {
	string oznaka;
	vector<Stajaliste> stajalista;
public:
	Linija(string oznaka) { this->oznaka = oznaka; }
	void parsiraj(string fileName) {
		ifstream file(fileName);
		string s;
		regex reg("\\[\"([^\\[]*)\\[#([0-9]*)\\];([^\"]*)\",([^,]*),([^,]*),([^\\]]*)\\].*");

		while (getline(file, s)) {
			smatch result;
			if (regex_match(s, result, reg)) {

				string naziv = result.str(1);
				int broj = atoi(result.str(2).c_str());
				double x = atof(result.str(4).c_str());
				double y = atof(result.str(5).c_str());
				string lin = result.str(3);

				set<Linija> linije;
				regex lreg("[ ]*([^\\|]*) \\|");
				sregex_iterator next(lin.begin(), lin.end(), lreg);
				sregex_iterator end;
				while (next != end) {
					smatch sm = *next;
					linije.insert(Linija(sm.str(1)));
					next++;
				}
				dodaj(Stajaliste(broj, naziv, x, y, linije));
			}
			else {
				cout << "No match" << endl;
			}
		}
		file.close();
	}
	string getOznaka()const { return oznaka; }
	void dodaj(Stajaliste s) { stajalista.push_back(s); }
	friend ostream& operator<<(ostream &ot, const Linija &l) {
		for (Stajaliste s : l.stajalista) ot << s;
		return ot;
	}
	friend bool operator<(const Linija &l1, const Linija &l2) { return l1.oznaka < l2.oznaka; }
	set<Linija> linije() {
		set<Linija> lin;
		for (Stajaliste s : stajalista) {
			for (Linija l : s.linije) {
				lin.insert(l);
			}
		}
		return lin;
	}
	vector<Stajaliste> ivicna() {
		vector<Stajaliste> v;
		v.push_back(*max_element(stajalista.begin(), stajalista.end(), [](const Stajaliste &a, const Stajaliste &b)->bool {return a.x < b.x; }));
		v.push_back(*max_element(stajalista.begin(), stajalista.end(), [](const Stajaliste &a, const Stajaliste &b)->bool {return a.y < b.y; }));
		v.push_back(*max_element(stajalista.begin(), stajalista.end(), [](const Stajaliste &a, const Stajaliste &b)->bool {return a.x > b.x; }));
		v.push_back(*max_element(stajalista.begin(), stajalista.end(), [](const Stajaliste &a, const Stajaliste &b)->bool {return a.y > b.y; }));
		return v;
	}
	pair<Linija, int> najvise()const {
		map<Linija, int> m;
		for (Stajaliste s : stajalista) {
			for (Linija l : s.linije) {
				if (l.oznaka == oznaka) continue;
				auto iter = m.find(l);
				if (iter == m.end()) {
					m[l] = 1;
				}
				else {
					m[l] = m[l] + 1;
				}
			}
		}
		using pt = pair<Linija, int>;
		return *max_element(m.begin(), m.end(), [](pt a, pt b)->bool {return a.second < b.second; });
	}
	map< pair<Linija, Linija>, int> zajednicka(int n = -1) {
		map< pair<Linija, Linija>, int> m;

		for (Stajaliste st : stajalista) {
			for (Linija l1 : st.linije) {
				for (Linija l2 : st.linije) {
					if (l1.oznaka >= l2.oznaka) continue;
					if (l1.oznaka == oznaka || l2.oznaka == oznaka) continue;
					pair<Linija, Linija> pair = make_pair(l1, l2);
					if (m.find(pair) != m.end()) {
						m[pair] = m[pair] + 1;
					}
					else {
						m[pair] = 1;
					}
				}
			}
		}
		if (n < 0) return m;
		for (auto iter = m.begin(); iter != m.end(); ) {
			if (iter->second < n) iter = m.erase(iter);
			else iter++;
		}
		return m;
	}
};

ostream& operator<<(ostream &ot, const Stajaliste &s) {
	ot << setw(6) << s.broj << "   " <<
		setw(45) << left << s.naziv << right <<
		setw(4) << fixed << setprecision(4) << s.x << ", " <<
		setw(4) << fixed << setprecision(4) << s.y << "     " << " { ";

	for (Linija l : s.linije) ot << l.getOznaka() << "  ";
	ot << "}" << endl;
	return ot;
}

int main() {
	Linija lin_74("74");
	lin_74.parsiraj("C:\\Users\\ziza\\Google Drive\\ETF - saradnik\\POOP\\lab1\\linija_74.txt");

	cout << lin_74;

	cout << endl << endl << "Linija 74 ima zajednicka stajalista sa sledecim linijama: " << endl;
	for (Linija s : lin_74.linije()) {
		cout << s.getOznaka() << "  ";
	}

	vector<Stajaliste> ivicna = lin_74.ivicna();
	cout << endl << endl << "Najsevernije: \t\t" << ivicna[0].getNaziv() << endl
		<< "Najistocnije: \t\t" << ivicna[1].getNaziv() << endl
		<< "Najjuznije: \t\t" << ivicna[2].getNaziv() << endl
		<< "Najzapadnije: \t\t" << ivicna[3].getNaziv() << endl;

	pair<Linija, int> naj = lin_74.najvise();
	cout << endl << endl << "Linija sa kojom linija 74 ima najvise zajednickih stajalista je linija " << naj.first.getOznaka()
		<< " i to " << naj.second << " stajalista..." << endl;

	map< pair<Linija, Linija>, int> zaj = lin_74.zajednicka(8);
	cout << endl << endl << "Zajednicka stajalista imaju linije: " << endl;
	for (auto iter : zaj) {
		cout << iter.first.first.getOznaka() << "\t" << iter.first.second.getOznaka() << "\t(" << iter.second << " stajalista)" << endl;
	}
	
	cout << endl << endl;
	system("pause");
	return 0;
}