#include <iostream>
using namespace std;

class Tacka {
	int x, y;
public:
	Tacka(int x = 0, int y = 0) { this->x = x; this->y = y; }
	friend std::ostream& operator<<(std::ostream &o, const Tacka &t) {
		return o << "(" << t.x << ", " << t.y << ")";
	}
};

class MojIterator: public iterator<input_iterator_tag, Tacka> {
	Tacka *tacke;
public:
	MojIterator(Tacka *tacke) { this->tacke = tacke; }
	MojIterator(const MojIterator &it) :tacke(it.tacke) {}
	MojIterator& operator++() { --tacke; return *this; }
	MojIterator operator++(int) {
		MojIterator tmp(*this); operator++(); return tmp;
	}
	bool operator==(const MojIterator &iter) { return tacke == iter.tacke; }
	bool operator!=(const MojIterator &iter) { return tacke != iter.tacke; }
	Tacka& operator*() { return *tacke; }
};

class Izlomljena {
	Tacka *tacke;
	int n; int kap;
public:
	Izlomljena(int n) { tacke = new Tacka[this->n = n]; }
	~Izlomljena() { delete tacke; }
	bool dodaj(Tacka t) {
		if (kap == n) return false;
		tacke[kap++] = t;
	}
	MojIterator begin() { return (tacke + kap - 1); }
	MojIterator end() { return (tacke - 1); }
};

void main() {
	Izlomljena i(4);
	i.dodaj(Tacka());
	i.dodaj(Tacka(1, 3));
	i.dodaj(Tacka(4, 3));
	i.dodaj(Tacka(9, 7));
	
	for (auto e : i) {
		cout << e << endl;
	}

	system("pause");
}