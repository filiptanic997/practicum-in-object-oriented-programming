#pragma once
#include <list>
#include <algorithm>
#include <iostream>
#include "Stajaliste.h"

class StajalistePostoji {
private:
	std::string naziv;
public:
	friend std::ostream & operator <<(std::ostream & out, const StajalistePostoji & s);

	StajalistePostoji(std::string nnaziv) : naziv(nnaziv) {}
};

class Smer {
private:
	std::list<Stajaliste *> stajalista;//contains pointers to Stajaliste, identified by it's id

public:
	const std::list<Stajaliste *> & dohvStajalista() const { return stajalista; }

	void dodajStajaliste(Stajaliste * s, int idNakon = -1) noexcept(false);
	void ukloniStajaliste(Stajaliste * s);

	friend std::ostream & operator <<(std::ostream & out, const Smer & s);

	Smer() {}
	~Smer() {}
};