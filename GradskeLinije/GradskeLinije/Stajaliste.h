#pragma once
#include <string>
#include <unordered_map>
#include "Lokacija.h"

class Linija;

class Stajaliste
{
private:
	int id;
	std::string naziv;
	Lokacija lokacija;
	std::list<Linija *> linije;
	int zona;

public:
	int dohvID() const { return id; }
	std::string dohvNaziv() const { return naziv; }
	const Lokacija & dohvLokaciju() const { return lokacija; }
	int dohvBrLinija() const { return linije.size(); }
	const std::list<Linija *> & dohvLinije() const { return linije; }
	int dohvZonu() const { return zona; }

	void dodajLiniju(Linija * l);
	void ukloniLiniju(Linija * l);

	friend bool operator ==(const Stajaliste& s1, const Stajaliste& s2);
	friend std::ostream & operator <<(std::ostream & out, const Stajaliste & s);

	Stajaliste(int iid, std::string nnaziv, double xCoordinate, double yCoordinate, int zzona)
		: id(iid), naziv(nnaziv), lokacija(xCoordinate, yCoordinate), zona(zzona) {}

	//removes itself from all Linija objects
	~Stajaliste();//implemented in .cpp because Linija is not yet defined
};