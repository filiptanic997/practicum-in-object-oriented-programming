#pragma once
#include <iostream>

class Lokacija
{
private:
	double xCoord;
	double yCoord;

public:
	friend bool operator ==(const Lokacija& l1, const Lokacija& l2);
	friend std::ostream & operator <<(std::ostream & out, const Lokacija & l);
	
	static double udaljenost(const Lokacija& l1, const Lokacija& l2);

	Lokacija(double xCoordinate, double yCoordinate) : xCoord(xCoordinate), yCoord(yCoordinate) {}
	~Lokacija() {}
};