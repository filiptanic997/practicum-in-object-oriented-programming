#pragma once
#include <unordered_set>
#include <map>

class Format {
public:
	virtual std::string prebaci(const std::multimap<std::string, std::string> & graf) const = 0;
};

class Format_GML : public Format {
private:
	virtual std::string prebaci(const std::multimap<std::string, std::string> & graf) const override
	{
		std::string out = "graph\n[";
		std::unordered_set<std::string> cvorovi;

		for (auto iter = graf.begin(); iter != graf.end(); iter++)
		{
			if (cvorovi.find(iter->first) != cvorovi.end())
				continue;

			cvorovi.insert(iter->first);
			out += "\n\tnode\n\t[\n\t id " + iter->first + "\n\t label \"" + iter->first + "\"\n\t]";
		}

		for(auto iter = graf.begin(); iter != graf.end(); iter++)
		{
			out += "\n\tedge\n\t[\n\t source " + iter->first + "\n\t target " + iter->second + "\n\t]";
		}

		out += "\n]";

		return out;
	}
};

class Format_CSV_Edges : public Format {
private:
	virtual std::string prebaci(const std::multimap<std::string, std::string> & graf) const override
	{
		std::string out = "";

		for (auto grana : graf) {
			out += grana.first + ";" + grana.second + "\n";
		}

		return out;
	}
};

class Format_CSV_List : public Format {
private:
	virtual std::string prebaci(const std::multimap<std::string, std::string> & graf) const override
	{
		std::string out = "";
		std::unordered_set<std::string> cvorovi;

		for (auto grana : graf) {
			if (cvorovi.find(grana.first) == cvorovi.end())
				cvorovi.insert(grana.first);
		}

		for (auto cvor : cvorovi) {
			auto rezultat = graf.equal_range(cvor);
			out += cvor;

			for (auto iter = rezultat.first; iter != rezultat.second; iter++)
				out += ";" + (*iter).second;

			out += "\n";
		}

		return out;
	}
};