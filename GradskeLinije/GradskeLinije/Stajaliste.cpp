#include "Stajaliste.h"
#include "Linija.h"

bool operator==(const Stajaliste & s1, const Stajaliste & s2)
{
	return (s1.lokacija == s2.lokacija) && (s1.naziv == s2.naziv);
}

std::ostream & operator<<(std::ostream & out, const Stajaliste & s)
{
	return out << s.id << " - " << s.naziv << " - " << s.lokacija << " - " << s.zona;
}

void Stajaliste::dodajLiniju(Linija * l)
{
	linije.push_back(l);
}

void Stajaliste::ukloniLiniju(Linija * l)
{
	std::string nazivLinije = l->dohvNaziv();
	auto iter = std::remove_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		return nazivLinije == l->dohvNaziv();
	});
	linije.erase(iter, linije.end());
}

Stajaliste::~Stajaliste()
{
	for (auto l : linije) {
		l->ukloniStajaliste(this);
	}
}