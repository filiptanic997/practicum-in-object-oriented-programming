#include "Mreza.h"
#include "Generator.h"
#include "Format.h"
#include <map>
#include <queue>
#include <set>

void Mreza::obrisiLiniju(std::string nazivLinije)
{
	auto iter = std::remove_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		if (nazivLinije == l->dohvNaziv()) {
			delete l;
			return true;
		}
		return false;
	});
	linije.erase(iter, linije.end());
}

void Mreza::prikaziLiniju(std::string nazivLinije) const noexcept(false)
{
	auto findIter = std::find_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		return nazivLinije == l->dohvNaziv();
	});
	if (findIter != linije.end())
		std::cout << **findIter;
	else
		throw PodaciNePostoje("Linija " + nazivLinije + " ne postoji u mrezi!");
}

void Mreza::prikaziStajaliste(int idStajalista) const noexcept(false)
{
	auto findIter = std::find_if(svaStajalista.begin(), svaStajalista.end(),
		[idStajalista](const std::pair<int, Stajaliste *>& s) -> bool {
		return idStajalista == s.first;
	});
	if (findIter != svaStajalista.end()) {
		std::cout << *((*findIter).second) << std::endl;
		for (auto l : (*findIter).second->dohvLinije())
			std::cout << l->dohvNaziv() << std::endl;
	}
	else
		throw PodaciNePostoje("Stajaliste sa brojem " + std::to_string(idStajalista) + " ne postoji u mrezi");
}

void Mreza::izmeniNazivLinije(std::string nazivLinije, std::string noviNaziv) noexcept(false)
{
	auto findIter = std::find_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		return nazivLinije == l->dohvNaziv();
	});

	if (findIter != linije.end())
		(*findIter)->postNaziv(noviNaziv);
	else
		throw PodaciNePostoje("Linija " + nazivLinije + " ne postoji u mrezi");
}

bool Mreza::postojiStajaliste(int idStajalista) const
{
	return svaStajalista.find(idStajalista) != svaStajalista.end();
}

void Mreza::dodajStajaliste(std::string nazivLinije, bool smer, int idStajalista, int idNakon, Stajaliste * s)noexcept(false)
{
	if (s == nullptr)
		s = (*svaStajalista.find(idStajalista)).second;
	else
		svaStajalista.insert(std::make_pair(idStajalista, s));

	auto iter = std::find_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		return nazivLinije == l->dohvNaziv();
	});

	if (iter == linije.end())
		throw PodaciNePostoje("Greska! Zadata linija ne postoji!");

	(*iter)->dodajStajaliste(s, smer, idNakon);
}

void Mreza::stvoriStajaliste(int id, std::string naziv, double xCoordinate, double yCoordinate, int zona) noexcept(false)
{
	if (svaStajalista.find(id) != svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem vec postoji!");

	svaStajalista.insert(std::make_pair(id, new Stajaliste(id, naziv, xCoordinate, yCoordinate, zona)));
}

void Mreza::ukloniStajaliste(std::string nazivLinije, int idStajalista, Stajaliste * s) noexcept(false)
{
	if (s == nullptr) {
		auto iter = svaStajalista.find(idStajalista);
		if (iter == svaStajalista.end())
			throw PodaciNePostoje("Greska! Zadato stajaliste ne postoji!");
		s = (*iter).second;
	}

	auto iter = std::find_if(linije.begin(), linije.end(),
		[nazivLinije](Linija * l) -> bool {
		return nazivLinije == l->dohvNaziv();
	});

	if (iter == linije.end())
		throw PodaciNePostoje("Greska! Zadata linija ne postoji!");

	(*iter)->ukloniStajaliste(s);
}

void Mreza::unistiStajaliste(int id) noexcept(false)
{
	auto iter = svaStajalista.find(id);

	if (iter == svaStajalista.end())
		throw PodaciNisuDobri("Upozorenje! Stajaliste sa zadatim ID-jem ne postoji!");

	delete (*iter).second;
	svaStajalista.erase(iter);
}

void Mreza::filterZona(int doZone)
{
	for (auto iter = linije.begin(); iter != linije.end(); ) {
		if (!(*iter)->linijaSamoDoZone(doZone)) {
			delete (*iter);
			linije.erase(iter++);
		}
		else
			iter++;
	}
}

void Mreza::filterOznaka(int doBroja)
{
	for (auto iter = linije.begin(); iter != linije.end(); ) {
		if ((*iter)->dohvBrojLinije() > doBroja) {
			delete (*iter);
			linije.erase(iter++);
		}
		else
			iter++;
	}
}

void Mreza::filterBrojStajalista(int brStajalista)
{
	for (auto iter = linije.begin(); iter != linije.end(); ) {
		if ((*iter)->dohvBrojStajalista() == brStajalista) {
			delete (*iter);
			linije.erase(iter++);
		}
		else
			iter++;
	}
}

std::unordered_set<Linija*> Mreza::ukrstajuSe(std::string saLinijom) const noexcept(false)
{
	auto iter = std::find_if(linije.begin(), linije.end(), [saLinijom](Linija * l)-> bool {
		return saLinijom == l->dohvNaziv();
	});

	if (iter == linije.end())
		throw PodaciNisuDobri("Greska! Linija ne postoji!");

	return (*iter)->ukrstajuce();
}

bool Mreza::prolaziKrozOba(std::string linija, int idStajalista1, int idStajalista2) const noexcept(false)
{
	auto iter = std::find_if(linije.begin(), linije.end(), [linija](Linija * l) -> bool {
		return linija == l->dohvNaziv();
	});

	if (iter == linije.end())
		throw PodaciNisuDobri("Greska! Linija ne postoji!");
	
	return (*iter)->prolaziKrozOba(idStajalista1, idStajalista2);
}

std::pair<Linija*, int> Mreza::najviseZajednickih(std::string linija) const 
{
	auto iter = std::find_if(linije.begin(), linije.end(), [linija](Linija * l) -> bool {
		return linija == l->dohvNaziv();
	});

	if (iter == linije.end())
		throw PodaciNisuDobri("Greska! Linija ne postoji!");

	return (*iter)->najviseZajednickih();
}

Stajaliste* Mreza::najblizeStajaliste(double xCoordinate, double yCoordinate, std::string linija) const noexcept(false)
{
	Linija * l = nullptr;
	std::map<Stajaliste *, double> udaljenost;
	
	if (linija != "default")
	{
		auto iter = std::find_if(linije.begin(), linije.end(), [linija](Linija * l) -> bool {
			return linija == l->dohvNaziv();
		});

		if (iter == linije.end())
			throw PodaciNisuDobri("Greska! Linija ne postoji!");

		l = *iter;
	}

	for (auto s : svaStajalista) {
		if (l == nullptr)
			udaljenost[s.second] = Lokacija::udaljenost(s.second->dohvLokaciju(), Lokacija(xCoordinate, yCoordinate));
		else
		{
			auto linije = s.second->dohvLinije();
			if(linije.end() != std::find(linije.begin(), linije.end(), l))
				udaljenost[s.second] = Lokacija::udaljenost(s.second->dohvLokaciju(), Lokacija(xCoordinate, yCoordinate));
		}
	}
	
	using par = const std::pair<Stajaliste *, double>;

	auto ret = std::min_element(udaljenost.begin(), udaljenost.end(), [](par p1, par p2) -> bool {
		return p1.second < p2.second;
	});

	return (*ret).first;
}

std::map<std::pair<Linija*, Linija*>, int> Mreza::paroviLinija(int minZajednickih, int maxZajednickih) const 
{
	std::map<std::pair<Linija *, Linija *>, int> parovi;

	for(auto sPar : svaStajalista)
	{
		Stajaliste * s = sPar.second;

		for(auto l1 : s->dohvLinije())
			for (auto l2 : s->dohvLinije())
			{
				if (l1->dohvNaziv() >= l2->dohvNaziv())
					continue;

				std::pair<Linija *, Linija *> parL = std::make_pair(l1, l2);
				auto iter = parovi.find(parL);
				if (iter == parovi.end())
					parovi[parL] = 1;
				else
					parovi[parL] = parovi[parL] + 1;
			}
	}

	if (minZajednickih == -1)
		return parovi;

	for(auto iter = parovi.begin(); iter != parovi.end();)
	{
		if (iter->second < minZajednickih || iter->second > maxZajednickih) 
			iter = parovi.erase(iter);
		else
			iter++;
	}

	return parovi;
}

std::list<Linija*> Mreza::krozStajaliste(int idStajalista) const noexcept(false) 
{
	auto iter = svaStajalista.find(idStajalista);

	if (iter == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");

	return (*iter).second->dohvLinije();
}

std::unordered_set<Stajaliste*> Mreza::susednaStajalista(int idStajalista) const noexcept(false) 
{
	auto iter = svaStajalista.find(idStajalista);

	if(iter == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");

	Stajaliste * s = (*iter).second;
	std::unordered_set<Stajaliste *> susedi;

	for(auto l : s->dohvLinije())
	{
		Stajaliste * sledece = l->sledeceStajaliste(s);

		if (sledece == nullptr || susedi.find(sledece) != susedi.end())
			continue;

		susedi.insert(sledece);
	}

	return susedi;
}

int Mreza::potrebnoPresedanja(int idStajalista1, int idStajalista2) const noexcept(false) 
{
	auto iter1 = svaStajalista.find(idStajalista1);
	if (iter1 == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");
	Stajaliste * source = (*iter1).second;

	auto iter2 = svaStajalista.find(idStajalista2);
	if (iter2 == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");
	Stajaliste * target = (*iter2).second;

	if (idStajalista1 == idStajalista2)
		return 0;

	std::unordered_set<Linija *> dostupneLinije, krajnjeLinije;
	auto parovi = paroviLinija();

	for (auto l : source->dohvLinije())
		dostupneLinije.insert(l);
	for (auto l : target->dohvLinije())
		krajnjeLinije.insert(l);

	std::unordered_set<Linija *> tmp;

	for(int i = 0; ; i++)
	{
		tmp.clear();
		for (auto iterD = dostupneLinije.begin(); iterD != dostupneLinije.end(); iterD++)
			if (krajnjeLinije.find(*iterD) != krajnjeLinije.end()) {
				std::cout << (*iterD)->dohvNaziv() << std::endl;
				return i;
			}

		for (auto parIter = parovi.begin(), tmpIter = parovi.begin(); parIter != parovi.end(); )
		{
			if (dostupneLinije.find((*parIter).first.first) != dostupneLinije.end() &&
				dostupneLinije.find((*parIter).first.second) == dostupneLinije.end())
			{
				tmp.insert((*parIter).first.second);
				tmpIter = parIter++;
				parovi.erase(tmpIter);
				
				continue;
			}

			if (dostupneLinije.find((*parIter).first.second) != dostupneLinije.end() &&
				dostupneLinije.find((*parIter).first.first) == dostupneLinije.end())
			{
				tmp.insert((*parIter).first.first);
				tmpIter = parIter++;
				parovi.erase(tmpIter);

				continue;
			}
			
			parIter++;
		}

		for (auto iterT = tmp.begin(); iterT != tmp.end(); iterT++)
			dostupneLinije.insert(*iterT);
	}

}

std::stack<Stajaliste *> Mreza::potrebnoStajalista(int idStajalista1, int idStajalista2) const noexcept(false)
{
	auto iter1 = svaStajalista.find(idStajalista1);
	if (iter1 == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");
	Stajaliste * source = (*iter1).second;

	auto iter2 = svaStajalista.find(idStajalista2);
	if (iter2 == svaStajalista.end())
		throw PodaciNisuDobri("Greska! Stajaliste sa zadatim ID-jem ne postoji!");
	Stajaliste * target = (*iter2).second;

	std::stack<Stajaliste *> put;

	if (idStajalista1 == idStajalista2)
		return put;

	using prioPar = std::pair<Stajaliste *, int>;

	const Lokacija pocetnaLokacija = target->dohvLokaciju(), krajnjaLokacija = target->dohvLokaciju();
	auto cmpPrio = [pocetnaLokacija, krajnjaLokacija](prioPar s1, prioPar s2) -> bool {
		return s1.second > s2.second;
		
		/*double d1 = Lokacija::udaljenost(krajnjaLokacija, s1.first->dohvLokaciju()) + s1.second *
			Lokacija::udaljenost(pocetnaLokacija, s1.first->dohvLokaciju()) * 2.;
		double d2 = Lokacija::udaljenost(krajnjaLokacija, s2.first->dohvLokaciju()) + s2.second *
			Lokacija::udaljenost(pocetnaLokacija, s2.first->dohvLokaciju()) * 2.;

		return d1 > d2;*/
	};

	std::unordered_map<Stajaliste *, int> vidjenaStajalista;
	std::unordered_map<Stajaliste *, Stajaliste *> prethodnaStajalista;
	std::priority_queue<prioPar, std::vector<prioPar>, decltype(cmpPrio)> dostupnaStajalista(cmpPrio);
	dostupnaStajalista.push(prioPar(source, 0));
	vidjenaStajalista[source] = 0;
	prethodnaStajalista[source] = nullptr;

	while(dostupnaStajalista.size() > 0)
	{
		Stajaliste * cur = dostupnaStajalista.top().first; dostupnaStajalista.pop();

		for(auto l : cur->dohvLinije())
		{
			Stajaliste * nxt = l->sledeceStajaliste(cur);

			if (nxt == nullptr)
				continue;

			if (nxt == target) {//target reached
				//std::cout << vidjenaStajalista.size() << std::endl;//test only
				Stajaliste * prev = cur;
				put.push(nxt);
				put.push(prev);
				while (prev != source)
				{
					prev = prethodnaStajalista[cur];
					put.push(prev);
					cur = prev;
				}
				
				return put;
				//return vidjenaStajalista[cur] + 1;
			}

			if (vidjenaStajalista.find(nxt) != vidjenaStajalista.end()) {//already visited
				if (vidjenaStajalista[nxt] > vidjenaStajalista[cur] + 1) {
					vidjenaStajalista[nxt] = vidjenaStajalista[cur] + 1;
					prethodnaStajalista[nxt] = cur;
				}
				continue;
			}

			dostupnaStajalista.push(prioPar(nxt, vidjenaStajalista[cur] + 1));
			vidjenaStajalista[nxt] = vidjenaStajalista[cur] + 1;
			prethodnaStajalista[nxt] = cur;
		}
	}

	return put;//target unreachable
}

void Mreza::generisiUFajl(std::string file) const noexcept(false)
{
	generator->generisiUFajl(this, file);
}

Linija * Mreza::parsirajLiniju(std::ifstream & inFile, std::string nazivLinije) const noexcept(false)
{
	Linija * toRet = nullptr;
	std::string s;
	std::regex regex_linija("[ ]*([0-9a-zA-Z]*)!([^!]*)!([^!]*)!.*");
	std::smatch rezultat;

	while (getline(inFile, s)) {
		if (std::regex_match(s, rezultat, regex_linija)) {
			if (rezultat.str(1) == nazivLinije) {//found
				toRet = new Linija(rezultat.str(1), rezultat.str(2), rezultat.str(3));

				break;
			}
		}
		else
			throw PodaciNisuDobri(nazivLinije);
	}

	return toRet;
}

void Mreza::parsirajSmer(std::ifstream & inFile, Linija * l, bool smer) noexcept(false)
{
	if (l == nullptr)
		return;

	std::string s;
	std::regex regex_stajaliste("[ ]*([0-9]*)!([^!]*)!([0-9.]*)!([0-9.]*)!([0-9]*).*");
	std::smatch rezultat;

	while (getline(inFile, s)) {
		if (std::regex_match(s, rezultat, regex_stajaliste)) {
			int id = atoi(rezultat.str(1).c_str());

			if (svaStajalista.find(id) != svaStajalista.end()) {//that object is already created
				(*svaStajalista.find(id)).second->dodajLiniju(l);
				l->dodajStajaliste((*svaStajalista.find(id)).second, smer);
				continue;
			}
			else {
				std::string naziv = rezultat.str(2);
				double xCoord = atof(rezultat.str(3).c_str());
				double yCoord = atof(rezultat.str(4).c_str());
				int zona = atoi(rezultat.str(5).c_str());

				Stajaliste * st = new Stajaliste(id, naziv, xCoord, yCoord, zona);
				//st->dodajLiniju(l);
				//svaStajalista.insert(std::make_pair(id, s));
				svaStajalista[id] = st;//already checked, it will always insert
				l->dodajStajaliste(st, smer);
			}
			
		}
		else {
			throw PodaciNisuDobri("Nisu dobri podaci za liniju " + l->dohvNaziv() + "!");
		}
	}

}

void Mreza::postaviFormat(Format* noviFormat) const 
{
	generator->postaviFormat(noviFormat);
}

void Mreza::ucitajLiniju(std::string nazivLinije) noexcept(false)
{
	std::ifstream inFile;

	try {
		inFile.open(DEFAULT_PATH + std::string(DEFAULT_LINES));
		if (!inFile.is_open()) throw PodaciNePostoje("Ne postoji " + std::string(DEFAULT_LINES) + "!");

		Linija * linija = parsirajLiniju(inFile, nazivLinije);
		if (linija == nullptr) throw PodaciNePostoje("Ne postoje podaci za liniju " + nazivLinije + "!");
		inFile.close();

		inFile.open(DEFAULT_PATH + std::string("\\" + nazivLinije + "_dirA.txt"));
		if (!inFile.is_open()) throw PodaciNePostoje("Ne postoje podaci za smer A linije " + nazivLinije + "!");
		parsirajSmer(inFile, linija, true);
		inFile.close();

		inFile.open(DEFAULT_PATH + std::string("\\" + nazivLinije + "_dirB.txt"));
		if (!inFile.is_open()) throw PodaciNePostoje("Ne postoje podaci za smer B linije " + nazivLinije + "!");
		parsirajSmer(inFile, linija, false);
		inFile.close();

		//remove_if that Linija exists already
		auto iter = std::remove_if(linije.begin(), linije.end(),
			[nazivLinije](Linija * l) -> bool {
			if (nazivLinije == l->dohvNaziv()) {
				delete l;
				return true;
			}
			return false;
		});
		linije.erase(iter, linije.end());

		linije.push_back(linija);
	}
	catch (PodaciNePostoje) {
		inFile.close();
		throw;
	}
}

void Mreza::ucitajSveLinije() noexcept(false)
{
	std::ifstream inFile;
	std::ifstream SmerInFile;
	std::vector<std::string> naziviLinija;
	
	inFile.open(DEFAULT_PATH + std::string(DEFAULT_LINES));
	if (!inFile.is_open()) throw RadSaFajlom("Doslo je do greske u radu sa " + std::string(DEFAULT_LINES) + "!");

	Linija * l = nullptr;
	std::string s;
	std::regex regex_linija("[ ]*([0-9a-zA-Z]*)!([^!]*)!([^!]*)!.*");
	std::smatch rezultat;

	while (getline(inFile, s)) {
		try {
			if (std::regex_match(s, rezultat, regex_linija)) {
				l = new Linija(rezultat.str(1), rezultat.str(2), rezultat.str(3));

				SmerInFile.open(DEFAULT_PATH + std::string("\\" + l->dohvNaziv() + "_dirA.txt"));
				if (!SmerInFile.is_open()) throw PodaciNePostoje("Ne postoje podaci za smer A linije " + l->dohvNaziv() + "!");
				parsirajSmer(SmerInFile, l, true);
				SmerInFile.close();

				SmerInFile.open(DEFAULT_PATH + std::string("\\" + l->dohvNaziv() + "_dirB.txt"));
				if (!SmerInFile.is_open()) throw PodaciNePostoje("Ne postoje podaci za smer B linije " + l->dohvNaziv() + "!");
				parsirajSmer(SmerInFile, l, false);
				SmerInFile.close();

				//remove_if that Linija exists already
				std::string nazivLinije = l->dohvNaziv();
				auto iter = std::remove_if(linije.begin(), linije.end(),
					[nazivLinije](Linija * l) -> bool {
					if (nazivLinije == l->dohvNaziv()) {
						delete l;
						return true;
					}
					return false;
				});
				linije.erase(iter, linije.end());

				linije.push_back(l);
			}

		}
		catch (PodaciNePostoje) {
			SmerInFile.close();
			delete l;
			continue;
		}
	}

	inFile.close();
}

std::ostream & operator<<(std::ostream & out, const LinijaPostoji & l)
{
	return out << "Greska! Ta linija vec postoji";
}

std::ostream & operator<<(std::ostream & out, const PodaciNePostoje & p)
{
	return out << p.poruka;
}

std::ostream & operator<<(std::ostream & out, const PodaciNisuDobri & p)
{
	return out << p.poruka;
}

std::ostream& operator <<(std::ostream& out, const RadSaFajlom& r) 
{
	return out << r.poruka;
}