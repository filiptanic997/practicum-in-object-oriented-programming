#pragma once
#include <regex>
#include "Smer.h"
#include <unordered_set>

class Linija
{
private:
	Smer A, B;
	std::string naziv, pocetna, krajnja;

public:
	std::string dohvNaziv() const { return naziv; }
	void postNaziv(std::string noviNaziv) { naziv = noviNaziv; }
	int dohvBrojLinije() const;
	int dohvBrojStajalista() const { return A.dohvStajalista().size() + B.dohvStajalista().size(); }

	void dodajStajaliste(Stajaliste * s, bool smer, int idNakon = -1) noexcept(false);//smer is true for direction A, and false for direction B
	void ukloniStajaliste(Stajaliste * s);

	bool linijaSamoDoZone(int doZone) const;
	std::unordered_set<Linija*> ukrstajuce() const;
	bool prolaziKrozOba(int idStajalista1, int idStajalista2) const;
	std::pair<Linija *, int> najviseZajednickih() const;
	Stajaliste * sledeceStajaliste(Stajaliste * s) const;


	friend std::ostream & operator <<(std::ostream & out, const Linija & l);

	Linija(std::string nnaziv, std::string ppocetna, std::string kkrajnja)
		: naziv(nnaziv), pocetna(ppocetna), krajnja(kkrajnja) {}

	//removes itself from all Stajaliste objects
	~Linija() {
		for (auto s : A.dohvStajalista())
			s->ukloniLiniju(this);
		for (auto s : B.dohvStajalista())
			s->ukloniLiniju(this);
	}
};
