#pragma once
#include "Linija.h"
#include <unordered_set>
#include <stack>
#include <regex>
#include <map>
#include <fstream>

const char * const DEFAULT_PATH = "..\\..\\primeri\\data";
const char * const DEFAULT_LINES = "\\_lines.txt";
const char * const DEBUG_PATH = "test_only.txt";

//currently not used
class LinijaPostoji {
private:
	std::string naziv;
public:
	friend std::ostream & operator <<(std::ostream & out, const LinijaPostoji & l);

	LinijaPostoji(std::string nnaziv) : naziv(nnaziv) {}
};
class PodaciNePostoje {
private:
	std::string poruka;
public:
	friend std::ostream & operator <<(std::ostream & out, const PodaciNePostoje & p);

	PodaciNePostoje(std::string pporuka) : poruka(pporuka) {}
};
class PodaciNisuDobri {
private:
	std::string poruka;
public:
	friend std::ostream & operator <<(std::ostream & out, const PodaciNisuDobri & p);

	PodaciNisuDobri(std::string pporuka) : poruka(pporuka) {}
};
class RadSaFajlom {
private:
	std::string poruka;
public:
	friend std::ostream& operator <<(std::ostream& out, const RadSaFajlom& r);

	RadSaFajlom(std::string pporuka) : poruka(pporuka) {}
};

class Generator;
class Format;

class Mreza
{
private:
	std::list<Linija *> linije;
	std::unordered_map<int, Stajaliste *> svaStajalista;//ensures the uniqueness of every Stajaliste object
	mutable Generator * generator = nullptr;

	Linija * parsirajLiniju(std::ifstream & inFile, std::string nazivLinije) const noexcept(false);
	void parsirajSmer(std::ifstream & inFile, Linija * l, bool smer) noexcept(false);

public:
	const std::list<Linija *> & dohvLinije() const { return linije; }
	const std::unordered_map<int, Stajaliste *> & dohvSvaStajalista() const { return svaStajalista; }
	void postaviGenerator(Generator * noviGenerator) const { generator = noviGenerator; }
	void postaviFormat(Format * noviFormat) const;

	//consider to be able to search in other directories, not only the default one
	void ucitajLiniju(std::string nazivLinije) noexcept(false);//set to search the default /primeri directory
	void ucitajSveLinije();
	void obrisiLiniju(std::string nazivLinije);
	void prikaziLiniju(std::string nazivLinije) const noexcept(false);
	void prikaziStajaliste(int idStajalista) const noexcept(false);
	void izmeniNazivLinije(std::string nazivLinije, std::string noviNaziv) noexcept(false);
	bool postojiStajaliste(int idStajalista) const;
	void dodajStajaliste(std::string nazivLinije, bool smer, int idStajalista, int idNakon = -1, Stajaliste * s = nullptr) noexcept(false);//smer is true for direction A, false for direction B
	void stvoriStajaliste(int id, std::string naziv, double xCoordinate, double yCoordinate, int zzona) noexcept(false);
	void ukloniStajaliste(std::string nazivLinije, int idStajalista, Stajaliste * s = nullptr) noexcept(false);//smer is true for direction A, false for direction B
	void unistiStajaliste(int id) noexcept(false);
	void filterZona(int doZone);
	void filterOznaka(int doBroja);
	void filterBrojStajalista(int brStajalista);

	std::unordered_set<Linija *> ukrstajuSe(std::string saLinijom) const noexcept(false);//set of Linija that have at least one common Stajaliste
	bool prolaziKrozOba(std::string linija, int idStajalista1, int idStajalista2) const noexcept(false);
	std::pair<Linija *, int> najviseZajednickih(std::string linija) const noexcept(false);
	Stajaliste * najblizeStajaliste(double xCoordinate, double yCoordinate, std::string linija = "default") const noexcept(false);
	std::map<std::pair<Linija *, Linija *>, int> paroviLinija(int minZajednickih = -1, int maxZajednickih = INT_MAX) const;
	std::list<Linija *> krozStajaliste(int idStajalista) const noexcept(false);
	std::unordered_set<Stajaliste *> susednaStajalista(int idStajalista) const noexcept(false);
	int potrebnoPresedanja(int idStajalista1, int idStajalista2) const noexcept(false);//needs more work!
	std::stack<Stajaliste *> potrebnoStajalista(int idStajalista1, int idStajalista2) const noexcept(false);

	void generisiUFajl(std::string file) const noexcept(false);

	Mreza() {}

	~Mreza() {
		for (auto l : linije)
			delete l;
		for (auto s : svaStajalista)
			delete s.second;
	}
};