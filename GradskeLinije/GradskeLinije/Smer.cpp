#include "Smer.h"

std::ostream & operator<<(std::ostream & out, const StajalistePostoji & s)
{
	return out << "Greska! Stajaliste " << s.naziv << " vec postoji na toj liniji!";
}

void Smer::dodajStajaliste(Stajaliste * s, int idNakon) noexcept(false)
{
	if(idNakon == -1)
		stajalista.push_back(s);
	else {
		auto iter = std::find_if(stajalista.begin(), stajalista.end(),
			[idNakon](Stajaliste * stajaliste) -> bool {
			return stajaliste->dohvID() == idNakon;
		});

		stajalista.insert(++iter, s);
	}
}

void Smer::ukloniStajaliste(Stajaliste * s)
{
	auto iter = std::remove_if(stajalista.begin(), stajalista.end(),
		[s](Stajaliste * stajaliste) -> bool {
		return stajaliste == s;
	});
	stajalista.erase(iter, stajalista.end());
}

std::ostream & operator<<(std::ostream & out, const Smer & s)
{
	for (auto s : s.stajalista)
		out << *s << std::endl;

	return out;
}