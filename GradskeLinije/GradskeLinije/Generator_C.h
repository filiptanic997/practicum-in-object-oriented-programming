#pragma once
#include "Generator.h"

class Generator_C : public Generator {
public:
	virtual void generisiUFajl(const Mreza * const mreza, std::string imeFajla) const noexcept(false) override;
};

inline void Generator_C::generisiUFajl(const Mreza * const mreza, std::string imeFajla) const noexcept(false)
{
	if (mreza == nullptr)
		throw new PodaciNisuDobri("Greska! Mreza nije zadata!");
	if (poFormatu == nullptr)
		throw new PodaciNisuDobri("Greska! Format nije zadat!");

	std::ofstream file;
	file.open(imeFajla + ekstenzija());
	if (!file.is_open())
		throw RadSaFajlom("Greska! Doslo je do greske u radu sa " + imeFajla + "!");

	std::multimap<std::string, std::string> graf;

	for(auto par : mreza->paroviLinija())
	{
		std::string l1 = par.first.first->dohvNaziv();
		std::string l2 = par.first.second->dohvNaziv();

		graf.insert(std::make_pair(l1, l2));
	}

	file << poFormatu->prebaci(graf);

	file.close();
}