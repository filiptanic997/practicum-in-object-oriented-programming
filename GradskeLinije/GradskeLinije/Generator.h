#pragma once
#include "Mreza.h"
#include "Format.h"

class Generator {
protected:
	Format * poFormatu = nullptr;

	std::string ekstenzija() const { 
		if (poFormatu == nullptr)
			return "";

		if (dynamic_cast<Format_GML*>(poFormatu))
			return ".gml";
		
		return ".csv";
	}

public:
	void postaviFormat(Format * noviFormat) { poFormatu = noviFormat; }

	virtual void generisiUFajl(const Mreza * const mreza, std::string imeFajla) const noexcept(false) = 0;
	
};