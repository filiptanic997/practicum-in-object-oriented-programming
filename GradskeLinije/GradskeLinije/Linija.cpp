#include "Linija.h"
#include <map>

std::ostream & operator<<(std::ostream & out, const Linija & l)
{
	out << l.naziv << " - " << l.pocetna << " - " << l.krajnja << std::endl;
	out << "Smer A" << std::endl << l.A;
	out << "Smer B" << std::endl << l.B << std::endl;

	return out;
}

int Linija::dohvBrojLinije() const
{
	std::regex regex_linija("[ a-zA-Z]*([0-9]*).*");
	std::smatch rezultat;

	std::regex_match(naziv, rezultat, regex_linija);
	return atoi(rezultat.str(1).c_str());
}

void Linija::dodajStajaliste(Stajaliste * s, bool smer, int idNakon) noexcept(false)
{
	Smer & sm = smer ? A : B;

	sm.dodajStajaliste(s, idNakon);
	s->dodajLiniju(this);
}

void Linija::ukloniStajaliste(Stajaliste * s)
{
	A.ukloniStajaliste(s);
	B.ukloniStajaliste(s);

	s->ukloniLiniju(this);
}

bool Linija::linijaSamoDoZone(int doZone) const
{
	bool postoji = false;

	for (auto s : A.dohvStajalista()) {
		if (s->dohvZonu() <= doZone)
			postoji = true;
		if (s->dohvZonu() > doZone)
			return false;
	}

	for (auto s : B.dohvStajalista()) {
		if (s->dohvZonu() <= doZone)
			postoji = true;
		if (s->dohvZonu() > doZone)
			return false;
	}

	return postoji;
}

std::unordered_set<Linija*> Linija::ukrstajuce() const 
{
	std::unordered_set<Linija *> set;

	for(auto s : A.dohvStajalista())
	{
		for(auto l : s->dohvLinije())
		{
			if (l->dohvNaziv() != naziv && set.count(l) == 0)
				set.insert(l);
		}
	}

	for (auto s : B.dohvStajalista())
	{
		for (auto l : s->dohvLinije())
		{
			if (l->dohvNaziv() != naziv && set.find(l) != set.end())
				set.insert(l);
		}
	}

	return set;
}

bool Linija::prolaziKrozOba(int idStajalista1, int idStajalista2) const
{
	int cnt = 0;

	for(auto s : A.dohvStajalista())
	{
		if (s->dohvID() == idStajalista1 || s->dohvID() == idStajalista2)
			cnt++;
	}

	if (cnt == 2)
		return true;
	
	cnt = 0;
	for (auto s : B.dohvStajalista())
	{
		if (s->dohvID() == idStajalista1 || s->dohvID() == idStajalista2)
			cnt++;
	}

	if (cnt == 2)
		return true;

	return false;
}

std::pair<Linija *, int> Linija::najviseZajednickih() const 
{
	std::map<Linija *, int> zajednicke;

	for(auto s : A.dohvStajalista())
		for(auto l : s->dohvLinije())
		{
			if (l->dohvNaziv() == naziv)
				continue;

			auto iter = zajednicke.find(l);

			if (iter == zajednicke.end())
				zajednicke[l] = 1;
			else
				zajednicke[l] = zajednicke[l] + 1;
		}
		
	for (auto s : B.dohvStajalista())
		for (auto l : s->dohvLinije())
		{
			if (l->dohvNaziv() == naziv)
				continue;

			auto iter = zajednicke.find(l);

			if (iter == zajednicke.end())
				zajednicke[l] = 1;
			else
				zajednicke[l] = zajednicke[l] + 1;
		}

	using par = std::pair<Linija *, int>;

	return *std::max_element(zajednicke.begin(), zajednicke.end(), [](par p1, par p2) -> bool {
		return p1.second < p2.second;
	});
}

Stajaliste* Linija::sledeceStajaliste(Stajaliste* s) const 
{
	auto skup = A.dohvStajalista();
	auto iter = std::find(skup.begin(), skup.end(), s);

	if (iter != skup.end())
	{
		if (++iter != skup.end())
			return *iter;

		auto it = B.dohvStajalista().begin();
		if (it != B.dohvStajalista().end())
			return *it;

		return nullptr;
	}

	skup = B.dohvStajalista();
	iter = std::find(skup.begin(), skup.end(), s);

	if (iter != skup.end())
	{
		if (++iter != skup.end())
			return *iter;

		auto it = A.dohvStajalista().begin();
		if (it != A.dohvStajalista().end())
			return *it;

		return nullptr;
	}
}