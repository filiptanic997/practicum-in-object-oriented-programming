#include "Lokacija.h"

bool operator ==(const Lokacija& l1, const Lokacija& l2) {
	return (l1.xCoord == l2.xCoord) && (l1.yCoord == l2.yCoord);
}

std::ostream & operator<<(std::ostream & out, const Lokacija & l)
{
	return out << l.xCoord << " - " << l.yCoord;
}

bool operator !=(const Lokacija& l1, const Lokacija& l2) {
	return !(l1 == l2);
}

double Lokacija::udaljenost(const Lokacija & l1, const Lokacija & l2)
{
	return abs((l1.xCoord - l2.xCoord)*(l1.xCoord - l2.xCoord) + (l1.yCoord - l2.yCoord)*(l1.yCoord - l2.yCoord));
}
