#include "Mreza.h"
#include "Generator_L.h"
#include "Generator_C.h"
#include <stack>

using namespace std;

int main() 
{
	Mreza * mreza = new Mreza();
	Generator * generator = nullptr, * genL = new Generator_L(), * genC = new Generator_C();
	Format * format = nullptr, * gml = new Format_GML(), * csvE = new Format_CSV_Edges(), * csvL = new Format_CSV_List();

	int choice;
	bool end = false;
	string imeFajla, nazivLinije, pomocni;
	bool smer;
	int iid, iidNarednog; 
	string nnaziv; 
	double xCoordinate; 
	double yCoordinate; 
	int zzona;

	while (!end) 
	{
		cout << "1. Ucitavanje podataka" << endl;
		cout << "2. Prikaz podataka" << endl;
		cout << "3. Manipulacija linije" << endl;
		cout << "4. Manipulacija stajalista" << endl;
		cout << "5. Primeni filter" << endl;
		cout << "6. Napredne funkcije" << endl;
		cout << "7. Kreiranje izlaznog fajla" << endl;
		cout << "8. Izlaz" << endl;
		cin >> choice;
		system("cls");
		
		switch (choice)//main menu switch
		{
		case 1:////
			cout << "1. Ucitavanje cele mreze" << endl;
			cout << "2. Ucitavanje jedne linije" << endl;
			cin >> choice;
			system("cls");

			/*cout << "Iz kog fajla zelite da citate(fajl, putanja)? ";
			cin >> imeFajla;
			cin >> putanja;
			system("cls");*/

			switch (choice) {
			case 1:
				delete mreza;
				mreza = new Mreza();

				try {
					mreza->ucitajSveLinije();
				}
				catch (RadSaFajlom e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 2:
				cout << "Naziv linije? ";
				cin >> nazivLinije;
				system("cls");

				try {
					mreza->ucitajLiniju(nazivLinije);
				}
				catch (PodaciNePostoje e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;
			}

			break;
			
		case 2:////
			cout << "1. Prikaz linije" << endl;
			cout << "2. Prikaz stajalista" << endl;
			cin >> choice;
			system("cls");

			switch (choice) {
			case 1:
				cout << "Naziv linije? ";
				cin >> nazivLinije;
				system("cls");

				try {
					mreza->prikaziLiniju(nazivLinije);
					system("pause");
					system("cls");
				}
				catch (PodaciNePostoje e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 2:
				cout << "ID stajalista? ";
				cin >> choice;
				system("cls");

				try {
					mreza->prikaziStajaliste(choice);
					system("pause");
					system("cls");
				}
				catch (PodaciNePostoje e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;
			}

			break;

		case 3:////
			cout << "1. Izmena oznake" << endl;
			cout << "2. Dodavanje stajalista" << endl;
			cout << "3. Uklanjanje stajalista" << endl;
			cout << "4. Brisanje linije" << endl;
			cin >> choice;
			system("cls");

			cout << "Naziv linije? ";
			cin >> nazivLinije;
			system("cls");

			switch (choice) {
			case 1:
				cout << "Novi naziv linije? ";
				cin >> pomocni;
				system("cls");

				try {
					mreza->izmeniNazivLinije(nazivLinije, pomocni);
				}
				catch (PodaciNePostoje e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 2:
				cout << "Smer(A - 1, B - 0), ID, ID prethodnog? ";
				cin >> smer >> iid >> iidNarednog;
				system("cls");
				{
					try {
						mreza->dodajStajaliste(nazivLinije, smer, iid, iidNarednog, nullptr);
					}
					catch (PodaciNePostoje e) {
						cout << e << endl;
						system("pause");
						system("cls");
					}
				}
				break;

			case 3:
				cout << "ID stajalista? ";
				cin >> iid;
				system("cls");

				try {
					mreza->ukloniStajaliste(nazivLinije, iid);
				}
				catch (PodaciNePostoje e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 4:
				mreza->obrisiLiniju(nazivLinije);

				break;
			}

			break;

		case 4:////
			cout << "1. Dodaj stajaliste" << endl;
			cout << "2. Ukloni stajaliste" << endl;
			cin >> choice;
			system("cls");

			switch (choice) {
			case 1:
				cout << "ID, naziv(jedna rec maksimalno), X koordinata, y koordinata, zona?\n";
				cin >> iid >> pomocni >> xCoordinate >> yCoordinate >> zzona;
				system("cls");

				try {
					mreza->stvoriStajaliste(iid, pomocni, xCoordinate, yCoordinate, zzona);
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 2:
				cout << "ID stajalista? ";
				cin >> iid;
				system("cls");

				try {
					mreza->unistiStajaliste(iid);
				}
				catch (PodaciNisuDobri ex) {
					cout << ex << endl;
					system("pause");
					system("cls");
				}
				break;
			}

			break;

		case 5:////
			cout << "1. Po zoni" << endl;
			cout << "2. Po oznaci" << endl;
			cout << "3. Po broju stajalista" << endl;
			cin >> choice;
			system("cls");

			switch (choice) {
			case 1:
				cout << "Samo linije do zone? " << endl;
				cin >> choice;
				system("cls");

				mreza->filterZona(choice);
				break;

			case 2:
				cout << "Samo linije sa oznakom manjom od? " << endl;
				cin >> choice;
				system("cls");

				mreza->filterOznaka(choice);
				break;

			case 3:
				cout << "Samo linije sa razlicitim brojem stajalista od? " << endl;
				cin >> choice;
				system("cls");

				mreza->filterBrojStajalista(choice);
				break;
			}

			break;

		case 6:
			cout << "1. Skup linija sa zajednickim stajalistem" << endl;
			cout << "2. Da li linija prolazi kroz oba stajalista(u istom smeru)" << endl;
			cout << "3. Linija sa najvise zajednickih stajalista" << endl;
			cout << "4. Najblize stajaliste po lokaciji" << endl;
			cout << "5. Parovi linija sa zajednickim stajalistima" << endl;
			cout << "6. Linije koje prolaze kroz stajaliste" << endl;
			cout << "7. Susedna stajalista" << endl;
			cout << "8. Najmanji broj presedanja" << endl;
			cout << "9. Najmanji broj stajalista" << endl;
			cin >> choice;
			system("cls");

			switch(choice) {
			case 1:
				cout << "Naziv linije? ";
				cin >> nazivLinije;
				system("cls");

				try {
					for (auto l : mreza->ukrstajuSe(nazivLinije))
						cout << l->dohvNaziv() << endl;

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 2:
				cout << "Naziv linije? ";
				cin >> nazivLinije;
				system("cls");

				cout << "ID prvog i drugog stajalista? ";
				cin >> choice >> iid;
				system("cls");

				try {
					if (mreza->prolaziKrozOba(nazivLinije, choice, iid))
						cout << "Prolazi" << endl;
					else
						cout << "Ne prolazi" << endl;

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 3:
				cout << "Naziv linije? ";
				cin >> nazivLinije;
				system("cls");

				try {
					cout << mreza->najviseZajednickih(nazivLinije).first->dohvNaziv() << ", " << mreza->najviseZajednickih(nazivLinije).second << "stajalista" << endl;
					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;
				
			case 4:
				cout << "X i Y koordinate? ";
				cin >> xCoordinate >> yCoordinate;
				system("cls");

				cout << "Naziv linije(n - bez linije)? ";
				cin >> nazivLinije;
				system("cls");

				try {
					if (nazivLinije == "n")
						cout << *mreza->najblizeStajaliste(xCoordinate, yCoordinate) << endl;
					else
						cout << *mreza->najblizeStajaliste(xCoordinate, yCoordinate, nazivLinije) << endl;
					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 5:
				cout << "Minimalan broj zajednickih? " << endl;
				cin >> choice;
				system("cls");

				for (auto par : mreza->paroviLinija(choice))
					cout << par.first.first->dohvNaziv() << " - " << par.first.second->dohvNaziv() << ", " << par.second << endl;

				system("pause");
				system("cls");
				break;

			case 6:
				cout << "ID prvog i drugog stajalista? ";
				cin >> choice >> iid;
				system("cls");

				try {
					for (auto l : mreza->krozStajaliste(choice))
						cout << l->dohvNaziv() << endl;

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 7:
				cout << "ID stajalista? ";
				cin >> choice;
				system("cls");

				try {
					for (auto s : mreza->susednaStajalista(choice))
						cout << s->dohvNaziv() << endl;

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 8:
				cout << "ID prvog i drugog stajalista? ";
				cin >> choice >> iid;
				system("cls");

				try {
					cout << mreza->potrebnoPresedanja(choice, iid) << endl;

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;

			case 9:
				cout << "ID prvog i drugog stajalista? ";
				cin >> choice >> iid;
				system("cls");

				try {
					stack<Stajaliste *> put = mreza->potrebnoStajalista(choice, iid);
					while (put.size() > 0) {
						cout << *put.top() << endl; put.pop();
					}

					system("pause");
					system("cls");
				}
				catch (PodaciNisuDobri e) {
					cout << e << endl;
					system("pause");
					system("cls");
				}
				break;
			}

			break;

		case 7:////
			cout << "1. L tip" << endl;
			cout << "2. C tip" << endl;
			cin >> choice;
			system("cls");

			if (choice == 1)
				generator = genL;
			else
				generator = genC;

			cout << "1. GML format" << endl;
			cout << "2. CSV_Edges format" << endl;
			cout << "3. CSV_List format" << endl;
			cin >> choice;
			system("cls");

			if (choice == 1)
				format = gml;
			else if (choice == 2)
				format = csvE;
			else
				format = csvL;

			cout << "Ime fajla(samo naziv fajla)? ";
			cin >> pomocni;
			system("cls");

			try {
				generator->postaviFormat(format);
				mreza->postaviGenerator(generator);

				mreza->generisiUFajl(pomocni);
			}
			catch (PodaciNisuDobri e) {
				cout << e << endl;
				system("pause");
				system("cls");
			}
			catch (RadSaFajlom e) {
				cout << e << endl;
				system("pause");
				system("cls");
			}

			break;

		default:
			end = true;

			break;
		}

	}

	return 0;
}