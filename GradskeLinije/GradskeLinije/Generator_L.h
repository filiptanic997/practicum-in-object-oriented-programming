#pragma once
#include "Generator.h"

class Generator_L : public Generator {
public:
	virtual void generisiUFajl(const Mreza * const mreza, std::string imeFajla) const noexcept(false) override;
};

inline void Generator_L::generisiUFajl(const Mreza * const mreza, std::string imeFajla) const noexcept(false) 
{
	if (mreza == nullptr)
		throw new PodaciNisuDobri("Greska! Mreza nije zadata!");
	if (poFormatu == nullptr)
		throw new PodaciNisuDobri("Greska! Format nije zadat!");

	std::ofstream file;
	file.open(imeFajla + ekstenzija());
	if (!file.is_open())
		throw RadSaFajlom("Greska! Doslo je do greske u radu sa " + imeFajla + "!");

	std::multimap<std::string, std::string> graf;

	for(auto s : mreza->dohvSvaStajalista())
	{
		std::string idCvora = std::to_string(s.second->dohvID());

		for(auto sused : mreza->susednaStajalista(s.second->dohvID()))
		{
			std::string idSusednog = std::to_string(sused->dohvID());
			
			graf.insert(std::make_pair(idCvora, idSusednog));
		}
	}

	file << poFormatu->prebaci(graf);

	file.close();
}