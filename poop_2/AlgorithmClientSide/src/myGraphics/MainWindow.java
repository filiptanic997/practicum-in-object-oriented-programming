package myGraphics;

import javafx.application.*;
import javafx.stage.Stage;
import distributed.*;
import java.util.*;
import java.util.concurrent.*;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.geometry.*;

public class MainWindow extends Application {

	private ConcurrentHashMap<String, AlgoClient> servers;

	public MainWindow() {
		servers = new ConcurrentHashMap();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		BorderPane rootPane = new BorderPane();

		Scene mainScene = new Scene(rootPane, 400, 200);

		VBox leftSide = new VBox();
		leftSide.prefWidthProperty().bind(mainScene.widthProperty().multiply(0.4));
		leftSide.setPadding(new Insets(10));
		leftSide.setSpacing(5);

		Label serverIP = new Label("Server IP Address:");
		serverIP.setPrefHeight(25);
		leftSide.getChildren().add(serverIP);
		TextField ipAddressField = new TextField("localhost");
		ipAddressField.setPrefHeight(25);
		ipAddressField.prefWidthProperty().bind(leftSide.widthProperty());
		leftSide.getChildren().add(ipAddressField);

		Label serverPort = new Label("Server port number:");
		serverPort.setPrefHeight(25);
		leftSide.getChildren().add(serverPort);
		TextField portField = new TextField("10");
		portField.setPrefHeight(25);
		portField.prefWidthProperty().bind(leftSide.widthProperty());
		leftSide.getChildren().add(portField);

		VBox buttons = new VBox();
		buttons.setAlignment(Pos.CENTER);
		buttons.setPadding(new Insets(leftSide.getSpacing(), 0, 0, 0));
		buttons.spacingProperty().bind(leftSide.spacingProperty().multiply(2));

		Button connect = new Button("Connect");
		connect.setPrefSize(100, 25);

		Button disconnect = new Button("Disconnect");
		disconnect.setPrefSize(100, 25);

		buttons.getChildren().addAll(connect, disconnect);

		leftSide.getChildren().add(buttons);

		rootPane.setLeft(leftSide);

		ListView<String> listOfServers = new ListView();
		listOfServers.prefWidthProperty().bind(mainScene.widthProperty().multiply(0.6));
		listOfServers.setEditable(true);
		rootPane.setRight(listOfServers);

		primaryStage.setScene(mainScene);
		primaryStage.setTitle("Algorithms Client");
		primaryStage.show();
		primaryStage.setResizable(false);

		connect.setOnAction(event -> {
			String ip = ipAddressField.getText();
			String portString = portField.getText();
			if (!ip.matches(
					"^(\\d|[1-9]\\d|1\\d\\d|2([0-4]\\d|5[0-5]))\\.(\\d|[1-9]\\d|1\\d\\d|2([0-4]\\d|5[0-5]))\\.(\\d|[1-9]\\d|1\\d\\d|2([0-4]\\d|5[0-5]))\\.(\\d|[1-9]\\d|1\\d\\d|2([0-4]\\d|5[0-5]))$")
					&& !ip.equals("localhost")) {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Error! Given IP address is not valid");
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
				return;
			}
			if (servers.containsKey(ip)) {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Error! Already connected to a server with the given IP address");
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
				return;
			}
			if (!portString.matches("[0-9]+")) {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Error! Not a decimal number");
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
				return;
			}
			int port = Integer.parseInt(portString);
			if (port < 0 || port >= 65535) {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Error! Server port is out of range [0, 65535]");
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
				return;
			}
			
			//not perfectly thread-safe!
			AlgoClient newServer = new GraphicalAlgoClient(listOfServers, servers, ip, port);
			servers.put(ip, newServer);
			newServer.start();
		});
		
		disconnect.setOnAction(event -> {
			for(String ip : listOfServers.getSelectionModel().getSelectedItems()) {
				servers.get(ip).stop();
			}
		});

		primaryStage.setOnCloseRequest(event -> {
			for (AlgoClient a : servers.values()) {
				a.stop();
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}

}
