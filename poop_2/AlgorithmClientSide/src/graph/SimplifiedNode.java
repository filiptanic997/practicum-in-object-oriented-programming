package graph;

import java.io.Serializable;

public class SimplifiedNode implements Serializable {

	private static final long serialVersionUID = 8189442421760010804L;
	public String nodeID;
	public double x;
	public double y;
	
	public SimplifiedNode(String nodeID, double x, double y) {
		this.nodeID = nodeID;
		this.x = x;
		this.y = y;
	}
	
	public void updatePosition(double xDisplacement, double yDisplacement) {
		x += xDisplacement;
		y += yDisplacement;
	}
}
