package distributed;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;

import java.util.concurrent.*;

public class GraphicalAlgoClient extends AlgoClient {

	private ListView<String> listOfServers;
	private ConcurrentHashMap<String, AlgoClient> servers;

	public GraphicalAlgoClient(ListView<String> listOfServers, ConcurrentHashMap<String, AlgoClient> servers,
			String serverIP, int portNumber) {

		super(serverIP, portNumber);
		this.listOfServers = listOfServers;
		this.servers = servers;
	}

	@Override
	protected void connect() {
	}

	@Override
	protected void success() {
		listOfServers.getItems().add(serverIP);
	}

	@Override
	protected void failed() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Error! Failed to conncect to server at " + serverIP);
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
			}
		});
	}

	@Override
	protected void disconnect() {
		servers.remove(serverIP);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				listOfServers.getItems().remove(serverIP);
			}
		});
	}

}
