package distributed;

import java.io.*;
import java.net.*;
import graph.*;
import java.util.*;

class Package implements Serializable {
	AlgoClient.Algorithm algorithm;
	HashMap<String, SimplifiedNode> graph;
}

public class AlgoClient implements Runnable {

	public enum Algorithm {
		Expansion, Contraction, ForceAtlas2, SalesMan
	};

	protected String serverIP;
	protected int portNumber;
	private Socket socket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private Thread t;

	private volatile HashMap<String, SimplifiedNode> graph;
	private volatile Algorithm algorithm;

	public AlgoClient(String serverIP, int portNumber) {
		this.serverIP = serverIP;
		this.portNumber = portNumber;
		t = new Thread(this);
		graph = new HashMap();
	}

	public void start() {
		t.start();
	}

	public void stop() {
		t.interrupt();
	}

	@Override
	public void run() {
		try {
			connect();
			System.out.println("attempting connection");
			socket = new Socket(serverIP, portNumber);// loophole?
			System.out.println("connected");
			inStream = new ObjectInputStream(socket.getInputStream());

			System.out.println("connected 2");
			success();

			/*while (!Thread.interrupted()) {// load the starting information
				try {
					Thread.sleep(10);
					algorithm = (Algorithm) inStream.readObject();
					break;
				} catch (EOFException e) {
					System.out.println("Waiting on algorithm!");
				}
			}*/

			while (!Thread.interrupted()) {
				try {
					Thread.sleep(10);
					graph = (HashMap<String, SimplifiedNode>) inStream.readObject();
					break;
				} catch (EOFException e) {
					System.out.println("Waiting on graph!");
				}
			}

			for (SimplifiedNode n : graph.values()) {
				System.out.println(n.nodeID + " " + n.x + " " + n.y);
			}

			double leftmost = Double.MAX_VALUE;
			double rightmost = Double.MIN_VALUE;
			double upmost = Double.MAX_VALUE;
			double downmost = Double.MIN_VALUE;

			for (SimplifiedNode n : graph.values()) {

				if (n.x < leftmost)
					leftmost = n.x;

				if (n.x > rightmost)
					rightmost = n.x;

				if (n.y < upmost)
					upmost = n.y;

				if (n.y > downmost)
					downmost = n.y;
			}

			double centerX = (leftmost + rightmost) / 2;
			double centerY = (upmost + downmost) / 2;
			
			outStream = new ObjectOutputStream(socket.getOutputStream());

			while (!Thread.interrupted()) {
				HashMap<String, SimplifiedNode> newGraph = new HashMap();
				
				for (SimplifiedNode n : graph.values()) {

					double xDisplacement = (n.x - centerX) * 0.00105;// sending additional information!
					double yDisplacement = (n.y - centerY) * 0.00105;
					
					n.updatePosition(xDisplacement, yDisplacement);
					newGraph.put(n.nodeID, new SimplifiedNode(n.nodeID, n.x, n.y));
					System.out.println(n.x + " " + n.y);
				}

				outStream.writeObject(newGraph);
				outStream.flush();
				graph = newGraph;
				Thread.sleep(10);
			}

		} catch (ConnectException e) {
			System.out.println("Exception1");
			failed();
		} catch (ClassCastException e) {
			System.out.println("Exception2");
			failed();
		} catch (Exception e) {
		}

		try {
			disconnect();
			socket.close();
		} catch (Exception e) {
		}

	}

	public static void main(String as[]) {
		new AlgoClient("localhost", 10);
	}

	protected void connect() {
	}

	protected void success() {
	}

	protected void failed() {
	}

	protected void disconnect() {
	}

}
