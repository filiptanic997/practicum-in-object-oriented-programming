package myUtil;

public class NodeDoesNotExistException extends Exception {

	public NodeDoesNotExistException(String message) {
		super(message);
	}
	
	public NodeDoesNotExistException() {
		super("No message!");
	}

	@Override
	public String toString() {
		return "NodeDoesNotExistException : " + getMessage();
	}
}
