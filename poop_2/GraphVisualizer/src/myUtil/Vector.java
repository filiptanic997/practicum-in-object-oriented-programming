package myUtil;

import javafx.geometry.Point2D;

public class Vector {

	private double originX;
	private double originY;
	private double endX;
	private double endY;

	public Vector(double originX, double originY, double endX, double endY) {
		super();
		this.originX = originX;
		this.originY = originY;
		this.endX = endX;
		this.endY = endY;
	}

	public synchronized double getXComponent() {
		return endX - originX;
	}

	public synchronized double getYComponent() {
		return endY - originY;
	}

	/** returns a value in degrees */
	public synchronized double getAngle() {
		return Math.atan2(endY - originY, endX - originX);
	}

	public synchronized void addVector(Vector vector) {
		endX += vector.getXComponent();
		endY += vector.getYComponent();
	}

	public synchronized Vector negate() {
		return new Vector(originX, originY, originX - getXComponent(), originY - getYComponent());
	}

	/** derivation */
	public synchronized void scaleBy(double scalingFactor) {
		endX = originX + getXComponent() * scalingFactor;
		endY = originY + getYComponent() * scalingFactor;
	}

	public synchronized Vector derivate(double delta) {
		return new Vector(originX, originY, originX + getXComponent() * delta, originY + getYComponent() * delta);
	}

	public synchronized void translateToEnd() {
		double x = getXComponent();
		double y = getYComponent();
		originX = endX;
		originY = endY;
		endX += x;
		endY += y;
	}

}
