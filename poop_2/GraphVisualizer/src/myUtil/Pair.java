package myUtil;

public class Pair<Left, Right> {
	
	private final Left argLeft;
	private final Right argRight;
	
	public Pair(Left argLeft, Right argRight) {
		super();
		this.argLeft = argLeft;
		this.argRight = argRight;
	}
	
	public Left getArgLeft() {
		return argLeft;
	}
	public Right getArgRight() {
		return argRight;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((argLeft == null) ? 0 : argLeft.hashCode());
		result = prime * result + ((argRight == null) ? 0 : argRight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (argLeft == null) {
			if (other.argLeft != null)
				return false;
		} else if (!argLeft.equals(other.argLeft))
			return false;
		if (argRight == null) {
			if (other.argRight != null)
				return false;
		} else if (!argRight.equals(other.argRight))
			return false;
		return true;
	}
	
	
}
