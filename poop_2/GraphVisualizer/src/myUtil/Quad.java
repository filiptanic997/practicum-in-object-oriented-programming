package myUtil;

public class Quad<Arg0, Arg1, Arg2, Arg3> {
	
	private final Arg0 arg0;
	private final Arg1 arg1;
	private final Arg2 arg2;
	private final Arg3 arg3;

	public Quad(Arg0 arg0, Arg1 arg1, Arg2 arg2, Arg3 arg3) {
		super();
		this.arg0 = arg0;
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
	}

	public Arg0 get0() {
		return arg0;
	}

	public Arg1 get1() {
		return arg1;
	}

	public Arg2 get2() {
		return arg2;
	}

	public Arg3 get3() {
		return arg3;
	}
	
}
