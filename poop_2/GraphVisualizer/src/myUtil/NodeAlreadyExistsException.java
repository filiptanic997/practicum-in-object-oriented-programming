package myUtil;

public class NodeAlreadyExistsException extends Exception {

	public NodeAlreadyExistsException(String message) {
		super(message);
	}
	
	public NodeAlreadyExistsException() {
		super("No message!");
	}

	@Override
	public String toString() {
		return "NodeAlreadyExistsException : " + getMessage();
	}
	
}
