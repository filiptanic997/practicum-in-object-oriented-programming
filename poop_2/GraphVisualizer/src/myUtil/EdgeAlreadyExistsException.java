package myUtil;

public class EdgeAlreadyExistsException extends Exception {
	
	public EdgeAlreadyExistsException(String message) {
		super(message);
	}
	
	public EdgeAlreadyExistsException() {
		this("No message!");
	}

	@Override
	public String toString() {
		return "EdgeAlreadyExistsException : " + getMessage();
	}
}
