package algorithm;

import graph.Graph;

public abstract class Algorithm extends Thread {

	protected volatile Boolean paused = true;
	protected Graph graph;
	
	public Algorithm(Graph graph) {
		super();
		this.graph = graph;
	}
	
	public synchronized Boolean getPaused() {
		return paused;
	}

	public synchronized void resumeAction() {
		paused = false;
		notifyAll();
	}

	public void pauseAction() {
		paused = true;
	}

	public void stopAction() {
		interrupt();
	}
	
	abstract public void setParams(double gravity, double scaleFactor);
}
