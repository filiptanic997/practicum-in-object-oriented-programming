package algorithm;

import graph.Graph;
import graph.Node;
import javafx.application.Platform;

public class Expansion extends Algorithm {

	private volatile double scaleFactor;

	public Expansion(Graph graph, double scaleFactor) {
		super(graph);
		this.scaleFactor = 0.001 * scaleFactor;
		this.setDaemon(true);
	}

	@Override
	public void run() {
		try {

			double leftmost = Double.MAX_VALUE;
			double rightmost = Double.MIN_VALUE;
			double upmost = Double.MAX_VALUE;
			double downmost = Double.MIN_VALUE;

			for (Node n : graph.getNodes()) {

				if (n.getCenterX() < leftmost)
					leftmost = n.getCenterX();

				if (n.getCenterX() > rightmost)
					rightmost = n.getCenterX();

				if (n.getCenterY() < upmost)
					upmost = n.getCenterY();

				if (n.getCenterY() > downmost)
					downmost = n.getCenterY();
			}

			double centerX = (leftmost + rightmost) / 2;
			double centerY = (upmost + downmost) / 2;

			while (!interrupted()) {

				synchronized (this) {
					while (paused)
						wait();
				}
				
				sleep(5);

				for (Node n : graph.getNodes()) {

					double xDisplacement = (n.getCenterX() - centerX) * scaleFactor;
					double yDisplacement = (n.getCenterY() - centerY) * scaleFactor;

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							n.updatePosition(xDisplacement, yDisplacement);
						}
					});
				}
			}
				
		} catch (InterruptedException e) {

		}
	}

	@Override
	public void setParams(double gravity, double scaleFactor) {
		this.scaleFactor = 0.001 * scaleFactor;
	}

}
