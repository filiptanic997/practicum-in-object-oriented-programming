package algorithm;

import java.util.*;
import graph.*;
import javafx.application.Platform;
import myUtil.Vector;

public class ForceAtlas2 extends Algorithm {

	private volatile double gravity;
	private volatile double scaling;

	public ForceAtlas2(Graph graph, double gravity, double scaling) {
		super(graph);
		this.gravity = gravity;
		this.scaling = scaling;
	}

	@Override
	public void run() {
		try {
			HashMap<Node, Vector> forces = new HashMap();
			HashMap<Node, Vector> prevVelocities = new HashMap();
			HashSet<Node> alreadyCalculated = new HashSet();

			while (!interrupted()) {
				synchronized (this) {
				while (paused)
					wait();
				}

				sleep(1);

				forces.clear();
				alreadyCalculated.clear();

				for (Node curNode : graph.getNodes()) {

					Vector curForce = forces.get(curNode);
					if (curForce == null) {
						curForce = new Vector(curNode.getCenterX(), curNode.getCenterY(), curNode.getCenterX(),
								curNode.getCenterY());
						forces.put(curNode, curForce);
					}

					for (Node neighbor : curNode.getNeighbors()) {

						if (alreadyCalculated.contains(neighbor))
							continue;

						Vector force = new Vector(curNode.getCenterX(), curNode.getCenterY(),
								curNode.getCenterX() + gravity / (neighbor.getCenterX() - curNode.getCenterX()),
								curNode.getCenterY() + gravity / (neighbor.getCenterY() - curNode.getCenterY()));
						curForce.addVector(force);

						Vector neighborForce = forces.get(neighbor);
						if (neighborForce == null) {
							neighborForce = new Vector(neighbor.getCenterX(), neighbor.getCenterY(),
									neighbor.getCenterX(), neighbor.getCenterY());
							forces.put(neighbor, neighborForce);
						}

						neighborForce.addVector(curForce.negate());
					}
					
					prevVelocities.put(curNode, new Vector(curNode.getCenterX(),
															curNode.getCenterY(),
															curNode.getCenterX(),
															curNode.getCenterY()));
					alreadyCalculated.add(curNode);
				}

				for (Node curNode : graph.getNodes()) {

					Vector curForce = forces.get(curNode);

					Vector prevVelocity = prevVelocities.get(curNode);
					
					Vector curVelocity = curForce.derivate(0.5);
					curVelocity.addVector(prevVelocities.get(curNode));
					curVelocity.scaleBy(0.5);
					prevVelocities.replace(curNode, curVelocity);

					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							curNode.updatePosition(curVelocity.getXComponent(), curVelocity.getYComponent());
						}
						
					});
					
				}

			}

		} catch (InterruptedException e) {
		}
	}

	@Override
	public void setParams(double gravity, double scaleFactor) {
		this.gravity = gravity;
		this.scaling = scaleFactor;
	}

}
