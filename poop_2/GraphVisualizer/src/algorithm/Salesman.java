package algorithm;

import graph.*;
import java.util.*;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class Salesman extends Algorithm {

	private Pane pane;
	private ArrayList<Node> path;
	private Node salesman;

	public Salesman(Pane pane, ArrayList<Node> path) {
		super(null);
		this.pane = pane;
		this.path = path;
		this.salesman = new Node("Salesman", path.get(0).getCenterX(),
				path.get(0).getCenterY(), 12.0, "label", Color.BLACK);
		pane.getChildren().add(this.salesman);
		setDaemon(true);
	}

	@Override
	public void setParams(double gravity, double scaleFactor) {
	}

	@Override
	public void run() {
		try {
			for(Node n : path) {
				System.out.println(n);
			}
			
			int targetNode = 1;
			double speedFactor = 250.;
			
			while(true) {
				
				Node target = path.get(targetNode++);
				
				double originalDistance = Node.distance(salesman, target);
				
				while(true) {
					double currentDistance = Node.distance(salesman, target);
					
					if(currentDistance <= 1.0)
						break;
					
					double divider = speedFactor * currentDistance / originalDistance;
					
					double xDisplacement = (target.getCenterX() - salesman.getCenterX()) / divider;
					double yDisplacement = (target.getCenterY() - salesman.getCenterY()) / divider;
					
					Platform.runLater(new Runnable(){
						@Override
						public void run() {
							salesman.updatePosition(xDisplacement, yDisplacement);
						}
					});
					
					sleep(10);
				}
			}
		}
		catch(InterruptedException e) {
			
		}
		catch(IndexOutOfBoundsException e) {
			
		}
		
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				pane.getChildren().remove(salesman);
			}
		});
		
	}

}
