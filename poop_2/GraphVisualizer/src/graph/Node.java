package graph;

import java.util.*;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import myGraphics.MainWindow;
import javafx.event.*;
import javafx.scene.input.*;

import myUtil.NodeAlreadyExistsException;

public class Node extends Circle {

	private static ArrayList<Node> selected = new ArrayList();
	private static Boolean labelVisible = true;
	private String id;
	HashMap<Node, Edge> neighbors = new HashMap();
	private double x;//unused
	private double y;//unused
	private static double dragStartX;
	private static double dragStartY;
	private Text textLabel;
	
	public Node(String id, double x, double y, double r, String textLabel, Color color) {
		this.id = id;
		this.x = x;
		setCenterX(x);
		setCenterY(y);
		this.y = y;
		setRadius(r);
		setFill(color);
		setSmooth(true);
		this.textLabel = new Text(textLabel);
		this.textLabel.setFill(Color.BLACK);
		this.textLabel.xProperty()
				.bind(this.centerXProperty().subtract(this.textLabel.getBoundsInLocal().getWidth() / 2));
		this.textLabel.yProperty().bind(this.centerYProperty().add(this.getBoundsInLocal().getHeight() / 16));
		this.textLabel.setVisible(labelVisible);

		addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if(event.isShiftDown()) {
				
			}
			else if (event.isControlDown() && selected.contains(this) && !event.isShiftDown())
				unselect((Node) event.getSource());
			else if (!event.isControlDown()) {
				unselectAll();
				select((Node) event.getSource());
			} else
				select((Node) event.getSource());

			dragStartX = event.getSceneX();
			dragStartY = event.getSceneY();
			event.consume();
		});

		addEventFilter(MouseEvent.MOUSE_DRAGGED, event -> {
			double scaleFactor = MainWindow.getCurrentScale();
			for (Node n : selected) {
				n.updatePosition((event.getSceneX() - dragStartX) * 1 / scaleFactor,
						(event.getSceneY() - dragStartY) * 1 / scaleFactor);
			}
			dragStartX = event.getSceneX();
			dragStartY = event.getSceneY();
			event.consume();
		});

	}

	public Node(String id, double x, double y, double r, String textLabel) {
		this(id, x, y, r, textLabel, Color.CORAL);
	}

	public Node(String id, double x, double y, double r) {
		this(id, x, y, r, id);
	}

	public Node(String id) {
		this(id, 100., 100., 5., id);
	}

	public static ArrayList<Node> getSelected() {
		return selected;
	}

	public static void unselect(Node node) {
		node.setStrokeWidth(0.0);
		selected.remove(node);
		//modif
		node.setOpacity(0.5);
		for(Edge e : node.neighbors.values()) {
			if(e.getNode1() == node && !selected.contains(e.getNode2()))
				e.setStroke(Color.BLACK);
			else if(e.getNode2() == node && !selected.contains(e.getNode1()))
				e.setStroke(Color.BLACK);
		}
		
		if(selected.isEmpty()) {
			for(Node n : Graph.mainGraph.getNodes()) {
				n.setOpacity(1.0);
			}
			for(Edge e : Graph.mainGraph.getEdges().values()) {
				e.setStroke(Color.BLACK);
			}
		}
	}

	public static void unselectAll() {
		for (Node n : selected) {
			n.setStrokeWidth(0.0);
		}
		selected.clear();
		//modif
		for(Node n : Graph.mainGraph.getNodes()) {
			n.setOpacity(1.0);
		}
		for(Edge e : Graph.mainGraph.getEdges().values()) {
			e.setStroke(Color.BLACK);
		}
	}

	public static void select(Node node) {
		if (!selected.contains(node)) {
			selected.add(node);
			node.setStroke(Color.DARKCYAN);
			node.setStrokeWidth(5.0);
			//modif
			node.setOpacity(1.0);
			for(Edge e : node.neighbors.values()) {
				e.setStroke(Color.RED);
			}
			
			if(selected.size() == 1) {
				for(Node n : Graph.mainGraph.getNodes()){
					if(n != node) {
						n.setOpacity(0.33);
					}
				}
			}
		}
	}

	public String getID() {
		return id;
	}

	public synchronized Set<Node> getNeighbors() {
		return neighbors.keySet();
	}

	@Deprecated
	public synchronized double getX() {
		return x;
	}

	@Deprecated
	public synchronized double getY() {
		return y;
	}

	public synchronized Node setSize(double size) {
		setRadius(size);

		return this;
	}

	public synchronized Text getTextLabel() {
		return textLabel;
	}

	public synchronized Node setLabelText(String newText) {
		textLabel.setText(newText);
		this.textLabel.xProperty()
				.bind(this.centerXProperty().subtract(this.textLabel.getBoundsInLocal().getWidth() / 2));
		this.textLabel.yProperty().bind(this.centerYProperty().add(this.getBoundsInLocal().getHeight() / 16));

		return this;
	}

	public synchronized Node setLabelSize(double newSize) {
		textLabel.setFont(new Font(newSize));
		this.textLabel.xProperty()
				.bind(this.centerXProperty().subtract(this.textLabel.getBoundsInLocal().getWidth() / 2));
		this.textLabel.yProperty().bind(this.centerYProperty().add(this.getBoundsInLocal().getHeight() / 16));

		return this;
	}

	public synchronized Node setLabelVisible(Boolean value) {
		textLabel.setVisible(value);
		labelVisible = value;

		return this;
	}

	public synchronized Node updatePosition(double xDisplacement, double yDisplacement) {
		setCenterX(getCenterX() + xDisplacement);
		setCenterY(getCenterY() + yDisplacement);
		for (Edge e : neighbors.values())
			e.redraw();

		return this;
	}

	public synchronized Node setPosition(double xPosition, double yPosition) {
		setCenterX(xPosition);
		setCenterY(yPosition);
		for (Edge e : neighbors.values())
			e.redraw();

		return this;
	}

	@Deprecated
	public synchronized void redraw() {
		setCenterX(x);
		setCenterY(y);
	}

	synchronized Node addNeighbor(Node node, Edge edge) throws NodeAlreadyExistsException {
		if (neighbors.containsKey(node))
			throw new NodeAlreadyExistsException("Node " + node.getID() + " is already a neighbor");

		neighbors.put(node, edge);

		return this;
	}

	synchronized Node removeNeighbor(Node node) {
		neighbors.remove(node);

		node.neighbors.remove(this);

		return this;
	}

	synchronized Node removeAllNeighbors() {
		for (Node neighbor : neighbors.keySet()) {
			neighbor.neighbors.remove(this);
		}

		neighbors.clear();

		return this;
	}

	public static double distance(Node node1, Node node2) {
		double x = node1.getCenterX() - node2.getCenterX();
		double y = node1.getCenterY() - node2.getCenterY();
		return Math.sqrt(x * x + y * y);
	}

	@Override
	public String toString() {
		return id;
	}

}
