package graph;

import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import myUtil.NodeAlreadyExistsException;
import myUtil.Quad;
import javafx.scene.text.*;

public class Edge extends Line{

	private static Boolean labelVisible = false;
	private final Node node1;
	private final Node node2;
	private Text textLabel;
	
	public Edge(Node node1, Node node2, String textLabel, Color color) {
		super();
		this.node1 = node1;
		this.node2 = node2;
		try {
			node1.addNeighbor(node2, this);
			node2.addNeighbor(node1, this);
		} catch (NodeAlreadyExistsException e) {}
		//this.setStroke(color);
		redraw();
		if(textLabel.equals(""))
			textLabel = node1.toString() + " to " + node2.toString();
		this.textLabel = new Text(textLabel);
		this.textLabel.setFill(color);
		this.textLabel.xProperty().bind(this.startXProperty().add(this.endXProperty().subtract(this.startXProperty()).divide(2.)));
		this.textLabel.yProperty().bind(this.startYProperty().add(this.endYProperty().subtract(this.startYProperty()).divide(2.)));
		this.textLabel.setVisible(labelVisible);
		
	}
	
	public Edge(Node node1, Node node2, String textLabel) {
		this(node1, node2, textLabel, Color.BLACK);
	}
	
	public Edge(Node node1, Node node2) {
		this(node1, node2, "");
	}

	public Node getNode1() {
		return node1;
	}

	public Node getNode2() {
		return node2;
	}

	public synchronized Text getTextLabel() {
		return textLabel;
	}
	
	public synchronized Edge setLabelText(String newText) {
		textLabel.setText(newText);
		this.textLabel.xProperty().bind(this.startXProperty().add(this.endXProperty().subtract(this.startXProperty()).divide(2.)));
		this.textLabel.yProperty().bind(this.startYProperty().add(this.endYProperty().subtract(this.startYProperty()).divide(2.)));
	
		return this;
	}
	
	public synchronized Edge setLabelVisible(Boolean value) {
		textLabel.setVisible(value);
		labelVisible = value;
		
		return this;
	}

	public void redraw() {
		setStartX(node1.getCenterX());
		setStartY(node1.getCenterY());
		setEndX(node2.getCenterX());
		setEndY(node2.getCenterY());
	}
	
	public synchronized Quad<Double, Double, Double, Double> getLineParams(){	
		return new Quad(node1.getX(), node1.getY(), node2.getX(), node2.getY());
	}

	@Override
	public String toString() {
		return "[" + node1 + " - " + node2 + "]";
	}
	
}
