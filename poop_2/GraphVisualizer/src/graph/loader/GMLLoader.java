package graph.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import graph.*;
import myUtil.*;

public class GMLLoader implements GraphLoader {

	@Override
	public void loadGraph(Graph graph, File file) {

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String strLine;
			br.readLine();
			br.readLine();
			while ((strLine = br.readLine()) != null) {
				Pattern p1 = Pattern.compile("^\\s*([a-z]+)");
				Matcher m1 = p1.matcher(strLine);
				
				if (m1.find()) {

					if (m1.group(1).contentEquals("node")) {
						br.readLine();
						String line = br.readLine();
						Pattern p2 = Pattern.compile("^\\s*id\\s(.+)");
						Matcher m2 = p2.matcher(line);
						
						if (m2.find()) {
							
							graph.addNode(new Node(m2.group(1)));
							line = br.readLine();
							p1 = Pattern.compile("^\\s*label\\s(.+)");
							m1 = p1.matcher(line);
							
							if (!m1.find())
								continue;
							else
								graph.getNode(m2.group(1)).setLabelText(m1.group(1));
						}
					}

					else if (m1.group(1).contentEquals("edge")) {
						br.readLine(); // [
						String line = br.readLine();
						Pattern p2 = Pattern.compile("^\\s*source\\s(.+)"); 
						Matcher m2 = p2.matcher(line);
						
						if (m2.find()) {
							String src = m2.group(1), dst = "";
							line = br.readLine();
							p2 = Pattern.compile("^\\s*target\\s(.+)");
							m2 = p2.matcher(line);
							
							if (m2.find()) {
								dst = m2.group(1);//src < dst checked in Graph.addNode() method
								
								try {
									graph.addEdge(src, dst);
								} catch(EdgeAlreadyExistsException e) {
									
								}
							}
							
							line = br.readLine();
							p2 = Pattern.compile("^\\s*label\\s\"(.+)\"");
							m2 = p2.matcher(line);
							if (!m2.find())
								continue; // if not matching continue
							else
								graph.getEdges().get(new Pair(src, dst)).setLabelText(m2.group(1));
						}

					}

					else
						break;
				}
				
				br.readLine();
			}
		} catch (Exception e) {
		}

	}

}
