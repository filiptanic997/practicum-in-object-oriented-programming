package graph.loader;

import graph.*;
import java.io.*;
import javafx.scene.paint.*;
import myUtil.NodeAlreadyExistsException;
import graph.Graph;

public class CustomLoader implements GraphLoader {

	@SuppressWarnings("deprecation")
	@Override
	public void loadGraph(Graph graph, File file) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			
			int numberOfNodes = Integer.parseInt(bufferedReader.readLine());
			int numberOfEdges = Integer.parseInt(bufferedReader.readLine());
			
			for(int i = 0; i < numberOfNodes; i++) {
				String line = bufferedReader.readLine();
				String[] nodeArgs = line.split(";");
				
				graph.addNode(new Node(nodeArgs[0],//nodeID
									Double.parseDouble(nodeArgs[1]),//x
									Double.parseDouble(nodeArgs[2]),//y
									Double.parseDouble(nodeArgs[3]),//r
									nodeArgs[4],//label
									new Color(Double.parseDouble(nodeArgs[5]),
											Double.parseDouble(nodeArgs[6]),
											Double.parseDouble(nodeArgs[7]), 1.0)));
			}
			
			for(int i = 0; i < numberOfEdges; i++) {
				String line = bufferedReader.readLine();
				String[] nodeArgs = line.split(";");
				
				
				graph.addEdge(nodeArgs[0], nodeArgs[1], nodeArgs[2],
							new Color(Double.parseDouble(nodeArgs[3]),
									Double.parseDouble(nodeArgs[4]),
									Double.parseDouble(nodeArgs[5]), 1.0));
			}
			
		} catch (FileNotFoundException e) {
			
		} catch (NumberFormatException e) {
			
		} catch (IOException e) {
			
		} catch (NodeAlreadyExistsException e) {
			
		}
	}

	@Override
	public void writeGraph(Graph graph, File file) {
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

			bufferedWriter.write(graph.getNodes().size() + "\n");// number of nodes
			bufferedWriter.write(graph.getEdges().values().size() + "\n");// number of edges

			for (Node node : graph.getNodes()) {

				bufferedWriter.write(node.getID() + ";" + node.getCenterX() + ";" + node.getCenterY() + ";"
						+ node.getRadius() + ";" + node.getTextLabel().getText() + ";" + ((Color)node.getFill()).getRed() + ";"
						+ ((Color)node.getFill()).getGreen() + ";" + ((Color)node.getFill()).getBlue() + "\n");
			}

			for (Edge e : graph.getEdges().values()) {
				bufferedWriter.write(e.getNode1() + ";" + e.getNode2()+ ";" + 
									e.getTextLabel().getText() + ";" + ((Color)e.getFill()).getRed() + ";" +
									((Color)e.getFill()).getGreen() + ";" + ((Color)e.getFill()).getBlue() + "\n");
			}
			bufferedWriter.close();
			
		} catch (Exception e) {
		}

	}

}
