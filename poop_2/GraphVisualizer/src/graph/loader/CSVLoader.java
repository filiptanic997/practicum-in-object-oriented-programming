package graph.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import graph.*;
import myUtil.*;

public class CSVLoader implements GraphLoader {

	@Override
	public void loadGraph(Graph graph, File file) {
		
		FileReader fileReader;
		BufferedReader buffReader = null;
		try {
			
			fileReader = new FileReader(file);
			buffReader = new BufferedReader(fileReader);

			String line;
			/*
			 * Pattern csvMatrix = Pattern.compile("^[^a-zA-Z0-9]{1}"); line =
			 * buffReader.readLine(); Matcher isMatrix = csvMatrix.matcher(line);
			 * if(isMatrix.matches()) {//csv format is matrix char delimiter =
			 * line.charAt(0); Pattern matrixHeader = Pattern.compile(delimiter +
			 * "([a-zA-Z0-9]+)"); Matcher header = matrixHeader.matcher(line); int nodeCount
			 * = 0; ArrayList<Node> nodes = new ArrayList(); while(header.find()) {
			 * nodeCount++; Node newNode = new Node(header.group(1)); nodes.add(newNode);
			 * try { graphWrapper.graph.addNode(newNode); } catch
			 * (NodeAlreadyExistsException e) {} }
			 * 
			 * } else {//csv format is lists or pairs buffReader.reset();
			 */
			Pattern csvList = Pattern.compile("(\\w+)\\W?");

			while ((line = buffReader.readLine()) != null) {
				Matcher matcher = csvList.matcher(line);
				if (matcher.find()) {
					String sourceNodeID = matcher.group(1);
					try {
						graph.addNode(sourceNodeID);
					} catch (NodeAlreadyExistsException e) {
					}

					while (matcher.find()) {
						String targetNodeID = matcher.group(1);
						try {
							graph.addEdge(sourceNodeID, targetNodeID);
						} catch (EdgeAlreadyExistsException e) {
						}
					}
				} else {
					throw new IOException();
				}
			}
			// }

		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		}
		
	}

}
