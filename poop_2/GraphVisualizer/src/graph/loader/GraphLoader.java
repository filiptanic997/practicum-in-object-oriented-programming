package graph.loader;

import java.io.File;
import graph.Graph;

public interface GraphLoader {
	
	void loadGraph(Graph graph, File file);
	
	default void writeGraph(Graph graph, File file) {
		
	}
	
}
