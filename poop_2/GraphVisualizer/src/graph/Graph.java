package graph;

import java.util.*;
import java.util.Map.Entry;
import javafx.scene.layout.*;
import myUtil.*;
import javafx.scene.paint.*;

public class Graph {

	private HashMap<String, Node> nodes = new HashMap();
	private HashMap<Pair<Node, Node>, Edge> edges = new HashMap();
	private Pane nodePane;
	private Pane edgePane;
	private Pane nodeLabelPane;
	private Pane edgeLabelPane;
	
	//modif
	public static Graph mainGraph = null;

	public Graph(Pane nodePane, Pane edgePane, Pane nodeLabelPane, Pane edgeLabelPane) {
		super();
		this.nodePane = nodePane;
		this.edgePane = edgePane;
		this.nodeLabelPane = nodeLabelPane;
		this.edgeLabelPane = edgeLabelPane;
		mainGraph = this;
	}

	public Graph() {
		this(null, null, null, null);
	}

	public synchronized void setPanes(Pane nodePane, Pane edgePane, Pane nodeLabelPane, Pane edgeLabelPane) {
		this.nodePane = nodePane;
		this.edgePane = edgePane;
		this.nodeLabelPane = nodeLabelPane;
		this.edgeLabelPane = edgeLabelPane;
	}
	
	public javafx.scene.layout.Pane getRootPane(){
		return (Pane) nodePane.getParent();
	}

	public synchronized Collection<Node> getNodes() {
		return nodes.values();
	}

	public synchronized HashMap<Pair<Node, Node>, Edge> getEdges() {
		return edges;
	}

	// *Adds an existing node into a graph*/
	public synchronized Graph addNode(Node toAdd) throws NodeAlreadyExistsException {
		if (nodes.containsKey(toAdd.getID()))
			throw new NodeAlreadyExistsException("Already added node " + toAdd.toString());

		nodes.put(toAdd.getID(), toAdd);

		if (nodePane != null) {
			nodePane.getChildren().add(toAdd);
			nodeLabelPane.getChildren().add(toAdd.getTextLabel());
		}

		return this;
	}

	// *Adds a completely new Node*/
	public synchronized Graph addNode(String nodeID) throws NodeAlreadyExistsException {
		if (nodes.containsKey(nodeID))
			throw new NodeAlreadyExistsException("Already added node " + nodeID);

		Node newNode = new Node(nodeID);
		nodes.put(nodeID, newNode);

		if (nodePane != null && nodeLabelPane != null) {
			nodePane.getChildren().add(newNode);
			nodeLabelPane.getChildren().add(newNode.getTextLabel());
		}

		return this;
	}

	// returns null
	public synchronized Node getNode(String nodeID) /* throws NodeDoesNotExistException */ {
		return nodes.get(nodeID);
	}
	
	public synchronized Graph updateName(String oldID, String newID) throws NodeAlreadyExistsException {
		Node target = nodes.get(newID);
		
		if(target != null)
			throw new NodeAlreadyExistsException("Node " + newID + " already eists in graph");
			
		target = nodes.get(oldID);
		nodes.remove(oldID);
		target.setLabelText(newID);
		nodes.put(newID, target);
		
		return this;
	}

	public synchronized Graph removeNode(Node toRemove) {
		if (toRemove != null) {
			if (nodePane != null && edgePane != null) {
				nodePane.getChildren().remove(toRemove);
				nodeLabelPane.getChildren().remove(toRemove.getTextLabel());
				for (Edge e : toRemove.neighbors.values()) {
					Node node1 = e.getNode1(), node2 = e.getNode2();
									
					edges.remove(new Pair<Node, Node>(node1, node2));
					edgePane.getChildren().remove(e);
					edgeLabelPane.getChildren().remove(e.getTextLabel());
				}
			}
			nodes.remove(toRemove.getID(), toRemove);
			toRemove.removeAllNeighbors();
		}

		return this;
	}

	public synchronized Graph removeNode(String nodeID) {
		Node target = nodes.get(nodeID);

		return removeNode(target);
	}

	// *Adds an edge from existing nodes into a graph*/
	public synchronized Graph addEdge(Node node1, Node node2)
			throws EdgeAlreadyExistsException, NodeDoesNotExistException {
		if (node1 == null || !nodes.containsValue(node1) || node2 == null || !nodes.containsValue(node2))
			throw new NodeDoesNotExistException("Given nodes do not exist in this graph");

		if (node1.getID().compareTo(node2.getID()) > 0) {
			Node temp = node1;
			node1 = node2;
			node2 = temp;
		}

		Pair pair = new Pair(node1, node2);
		if (edges.containsKey(pair))
			throw new EdgeAlreadyExistsException("Already added edge " + node1.getID() + "-" + node2.getID());

		Edge newEdge = new Edge(node1, node2);
		edges.put(pair, newEdge);

		if (edgePane != null && edgeLabelPane != null) {
			edgePane.getChildren().add(newEdge);
			edgeLabelPane.getChildren().add(newEdge.getTextLabel());
		}

		return this;
	}

	// *Adds an edge from new nodes into a graph*/
	public synchronized Graph addEdge(String node1ID, String node2ID) throws EdgeAlreadyExistsException {
		Node node1 = nodes.get(node1ID);
		if (node1 == null) {
			node1 = new Node(node1ID);
			nodes.put(node1ID, node1);
			if (nodePane != null) {
				nodePane.getChildren().add(node1);
				nodeLabelPane.getChildren().add(node1.getTextLabel());
			}
		}
		Node node2 = nodes.get(node2ID);
		if (node2 == null) {
			node2 = new Node(node2ID);
			nodes.put(node2ID, node2);
			if (nodePane != null) {
				nodePane.getChildren().add(node2);
				nodeLabelPane.getChildren().add(node2.getTextLabel());
			}
		}

		if(node1.getID().compareTo(node2.getID()) > 0) {
			Node temp = node1;
			node1 = node2;
			node2 = temp;
		}
		
		Pair pair = new Pair(node1, node2);
		if (edges.containsKey(pair)) {
			throw new EdgeAlreadyExistsException("Already added edge " + node1.getID() + "-" + node2.getID());
		}

		Edge newEdge = new Edge(node1, node2);
		edges.put(pair, newEdge);

		if (edgePane != null) {
			// System.out.println("Dodajem granu!");//DEBUG ONLY
			edgePane.getChildren().add(newEdge);
			edgeLabelPane.getChildren().add(newEdge.getTextLabel());
		}

		return this;
	}
	
	//utility, not for conventional use
	@Deprecated
	public synchronized Graph addEdge(String node1ID, String node2ID, String textLabel, Color color) {
		Node node1 = nodes.get(node1ID);
		Node node2 = nodes.get(node2ID);
		
		Edge e = new Edge(node1, node2, textLabel, color);
		
		edges.put(new Pair<Node, Node>(node1, node2), e);
		edgePane.getChildren().add(e);
		edgeLabelPane.getChildren().add(e.getTextLabel());
		
		return this;
	}

	public synchronized Graph removeEdge(Node node1, Node node2) {
		if (node1 != null && node2 != null) {
			node1.removeNeighbor(node2);
			if(node1.getID().compareTo(node2.getID()) > 0) {
				Node temp = node1;
				node1 = node2;
				node2 = temp;
			}
			Edge e = edges.get(new Pair(node1, node2));
			if(e == null) {
				return this;
			}
			if (edgePane != null) {
				edgePane.getChildren().remove(e);
				edgeLabelPane.getChildren().remove(e.getTextLabel());
			}
			edges.remove(new Pair(node1, node2));
			
		}

		return this;
	}
	
	/**Returns a path without start and end nodes*/
	public synchronized ArrayList<Node> findShortestPathBFS(Node node1, Node node2) {
		HashMap<Node, Node> prevs = new HashMap();
		ArrayList<Node> queue = new ArrayList();
		queue.add(node1);
		
		Boolean found = false;
		while(!queue.isEmpty()) {
			Node cur = queue.get(0);
			queue.remove(0);
			
			for(Node neighbor : cur.neighbors.keySet()) {
				if(prevs.containsKey(neighbor))
					continue;
				
				prevs.put(neighbor, cur);
				queue.add(neighbor);
				
				if(neighbor == node2) {
					found = true;
					break;
				}
			}
		}
		
		if(!found)
			return queue;
		
		queue.clear();
		Node cur = node2;
		while(true) {
			Node next = prevs.get(cur);
			
			if(next == node1)
				break;
			
			queue.add(0, next);
			cur = next;
		}
		
		return queue;
	}
	
	public synchronized ArrayList<Node> findShortestPathBFS(String node1ID, String node2ID) throws NodeDoesNotExistException {
		Node n1 = nodes.get(node1ID), n2 = nodes.get(node2ID);
		
		if(n1 == null || n2 == null)
			throw new NodeDoesNotExistException("There are no nodes iwth those IDs in the graph");
		
		return findShortestPathBFS(n1, n2);
	}

	/*public static void main(String args[]) {
		Graph g = new Graph();
		try {
			g.addEdge("27E", "27");
			Node n = new Node("55");
			g.addNode(n).addEdge("27E", "55");

			g.addEdge(g.getNode("27"), n);

			for (Node no : g.nodes.values()) {
				System.out.println(no);
			}
			for (Edge edge : g.edges.values()) {
				System.out.println(edge);
			}

			// g.addNode("27E");

		} catch (EdgeAlreadyExistsException e) {
			System.out.println(e);
		} catch (NodeAlreadyExistsException e) {
			System.out.println(e);
		} catch (NodeDoesNotExistException e) {
			System.out.println(e);
		}
	}*/

	public synchronized void clear() {
		nodes.clear();
		edges.clear();
		nodePane.getChildren().clear();
		edgePane.getChildren().clear();
		nodeLabelPane.getChildren().clear();
		edgeLabelPane.getChildren().clear();
	}

	public synchronized void formatNodeColor(Color lowBound, Color highBound) {
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(Node n : nodes.values()) {
			int degree = n.getNeighbors().size();
			if(degree > max)
				max = degree;
			if(degree < min)
				min = degree;
		}
		
		double range = max = min;
		double redRange = highBound.getRed() - lowBound.getRed();
		double greenRange = highBound.getGreen() - lowBound.getGreen();
		double blueRange = highBound.getBlue() - lowBound.getBlue();
		
		for(Node n : nodes.values()) {
			double coeficient = (n.getNeighbors().size() - min) / range;
			
			double red = lowBound.getRed() + redRange * coeficient;
			double green = lowBound.getGreen() + greenRange * coeficient;
			double blue = lowBound.getBlue() + blueRange * coeficient;
			n.setFill(new Color(red, green, blue, 1.0));
		}
	}
	
	public synchronized void formatNodeSize(int lowBound, int highBound) {
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(Node n : nodes.values()) {
			int degree = n.getNeighbors().size();
			if(degree > max)
				max = degree;
			if(degree < min)
				min = degree;
		}
		
		double range = max - min;
		for(Node n : nodes.values()) {
			double coeficient = (n.getNeighbors().size() - min) / range;
			
			n.setSize(lowBound + (highBound - lowBound) * coeficient);
		}
	}
	
	public synchronized void formatLabelSize(int lowBound, int highBound) {
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(Node n : nodes.values()) {
			int degree = n.getNeighbors().size();
			if(degree > max)
				max = degree;
			if(degree < min)
				min = degree;
		}
		
		double range = max - min;
		for(Node n : nodes.values()) {
			double coeficient = (n.getNeighbors().size() - min) / range;
			
			n.setLabelSize(lowBound + (highBound - lowBound) * coeficient);
		}
	}
	
}
