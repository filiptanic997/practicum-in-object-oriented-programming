package undoManagement;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import graph.Graph;
import graph.Node;
import myUtil.NodeAlreadyExistsException;

public class RenameNode extends AbstractUndoableEdit {

	private Graph graph;
	private String target;
	private String oldName;
	private String newName;

	public RenameNode(Graph graph, Node target, String newName) {
		this.graph = graph;
		this.target = target.getID();
		this.oldName = graph.getNode(this.target).getTextLabel().getText();
		this.newName = newName;
		redo();
	}

	@Override
	public void undo() throws CannotUndoException {
		graph.getNode(target).setLabelText(oldName);
	}

	@Override
	public void redo() throws CannotRedoException {
		graph.getNode(target).setLabelText(newName);
	}
}
