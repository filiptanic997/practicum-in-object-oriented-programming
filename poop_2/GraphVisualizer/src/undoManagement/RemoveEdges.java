package undoManagement;

import java.util.*;
import java.util.Map.Entry;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import graph.*;
import javafx.scene.paint.Color;
import myUtil.Pair;

public class RemoveEdges extends AbstractUndoableEdit {

	private Graph graph;
	private HashMap<Pair<String, String>, String> pairsAndLabels;

	public RemoveEdges(Graph graph, ArrayList<Node> selection) {
		this.graph = graph;
		this.pairsAndLabels = new HashMap();

		for (Node node1 : selection) {

			for (Node node2 : selection) {

				if (node1 == node2 || node1.getID().compareTo(node2.getID()) > 0
						|| graph.getEdges().get(new Pair(node1, node2)) == null)
					continue;

				pairsAndLabels.put(new Pair(node1.getID(), node2.getID()),
						graph.getEdges().get(new Pair(node1, node2)).getTextLabel().getText());
			}
		}
		redo();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void undo() throws CannotUndoException {
		for (Pair<String, String> key : pairsAndLabels.keySet()) {
			graph.addEdge(key.getArgLeft(), key.getArgRight(), pairsAndLabels.get(key), Color.BLACK);
		}
	}

	@Override
	public void redo() throws CannotRedoException {
		for (Pair<String, String> key : pairsAndLabels.keySet()) {
			graph.removeEdge(graph.getNode(key.getArgLeft()), graph.getNode(key.getArgRight()));
		}
	}

}
