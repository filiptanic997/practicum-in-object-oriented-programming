package undoManagement;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.undo.*;
import graph.*;
import javafx.geometry.*;

public class ResizeNodes extends AbstractUndoableEdit {
	
	private Graph graph;
	HashMap<String, Double> oldSizes;
	private double newSize;
	ArrayList<String> toResize;

	public ResizeNodes(Graph graph, ArrayList<Node> selection, double newSize) {
		this.graph = graph;
		this.newSize = newSize;
		oldSizes = new HashMap();
		toResize = new ArrayList();
		for(Node n : selection) {
			toResize.add(n.getID());
			oldSizes.put(n.getID(), n.getRadius());
		}
		redo();
	}

	@Override
	public synchronized void undo() throws CannotUndoException {
		for(String nodeID : toResize) {
			Node n = graph.getNode(nodeID);
			n.setSize(oldSizes.get(n));
		}
	}

	@Override
	public synchronized void redo() throws CannotRedoException {
		for(String nodeID : toResize) {
			Node n = graph.getNode(nodeID);
			n.setSize(newSize);
		}
	}

}
