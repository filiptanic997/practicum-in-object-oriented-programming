package undoManagement;

import java.util.ArrayList;
import javax.swing.undo.*;
import graph.*;
import javafx.geometry.*;

public class MoveNodes extends AbstractUndoableEdit {

	private Graph graph;
	private double offsetX;
	private double offsetY;
	ArrayList<String> toMove;

	public MoveNodes(Graph graph, ArrayList<Node> selection, Point2D start, Point2D end) {
		this.graph = graph;
		offsetX = end.getX() - start.getX();
		offsetY = end.getY() - start.getY();
		toMove = new ArrayList();
		for (Node n : selection) {
			toMove.add(n.getID());
		}
	}

	@Override
	public synchronized void undo() throws CannotUndoException {
		for (String nodeID : toMove) {
			Node n = graph.getNode(nodeID);
			n.updatePosition(-offsetX, -offsetY);
		}
	}

	@Override
	public synchronized void redo() throws CannotRedoException {
		for (String nodeID : toMove) {
			Node n = graph.getNode(nodeID);
			n.updatePosition(offsetX, offsetY);
		}
	}

}
