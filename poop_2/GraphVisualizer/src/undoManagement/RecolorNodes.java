package undoManagement;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.undo.*;
import graph.*;
import javafx.application.Platform;
import javafx.scene.paint.Color;

public class RecolorNodes extends AbstractUndoableEdit {

	private Graph graph;
	private ArrayList<String> toRecolor;
	private HashMap<String, Color> oldColors;
	private Color newColor;
	
	public RecolorNodes(Graph graph, ArrayList<Node> selection, Color newColor) {
		this.graph = graph;
		this.newColor = newColor;
		oldColors = new HashMap();
		toRecolor = new ArrayList();
		for(Node n : selection) {
			toRecolor.add(n.getID());
			oldColors.put(n.getID(), (Color)n.getFill());
		}
		redo();
	}

	@Override
	public synchronized void undo() throws CannotUndoException {
		for(String nodeID : toRecolor) {
			Node n = graph.getNode(nodeID);
			n.setFill(oldColors.get(nodeID));
		}
	}

	@Override
	public synchronized void redo() throws CannotRedoException {
		for(String nodeID : toRecolor) {
			Node n = graph.getNode(nodeID);
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					n.setFill(newColor);
				}
			});
			
		}
	}
	
}
