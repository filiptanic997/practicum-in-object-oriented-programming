package undoManagement;

import javax.swing.undo.*;
import graph.*;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonBar.ButtonData;
import myUtil.NodeAlreadyExistsException;

public class AddNode extends AbstractUndoableEdit {

	private Graph graph;
	private String nodeID;
	private double positionX;
	private double positionY;
	private double radius;
	

	public AddNode(Graph graph, String nodeID, double positionX, double positionY, double radius) {
		this.graph = graph;
		this.nodeID = nodeID;
		this.positionX = positionX;
		this.positionY = positionY;
		this.radius = radius;
		redo();
	}

	@Override
	public void undo() throws CannotUndoException {
		graph.removeNode(nodeID);
	}

	@Override
	public void redo() throws CannotRedoException {
		try {
			graph.addNode(new Node(nodeID, positionX, positionY, radius));
		} catch (NodeAlreadyExistsException e) {
			Dialog<Boolean> dialog = new Dialog();
			dialog.setTitle("Warning");
			dialog.setContentText("NodeID already exists");
			dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
			dialog.showAndWait();
		}
	}
	
}
