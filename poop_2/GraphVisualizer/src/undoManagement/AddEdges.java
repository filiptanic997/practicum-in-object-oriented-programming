package undoManagement;

import java.util.*;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import graph.Graph;
import graph.Node;
import myUtil.EdgeAlreadyExistsException;
import myUtil.NodeDoesNotExistException;
import myUtil.Pair;

public class AddEdges extends AbstractUndoableEdit {

	private Graph graph;
	private HashSet<Pair<String, String>> pairs;

	public AddEdges(Graph graph, ArrayList<Node> selection) {
		this.graph = graph;
		this.pairs = new HashSet();

		for (Node node1 : selection) {

			for (Node node2 : selection) {
				
				if(node1 == node2 || node1.getID().compareTo(node2.getID()) > 0 || graph.getEdges().get(new Pair(node1, node2)) != null)
					continue;

				pairs.add(new Pair(node1.getID(), node2.getID()));
			}
		}
		redo();
	}

	@Override
	public void undo() throws CannotUndoException {
		for(Pair<String, String> pair : pairs) {
			graph.removeEdge(graph.getNode(pair.getArgLeft()), graph.getNode(pair.getArgRight()));
		}
	}

	@Override
	public void redo() throws CannotRedoException {
		for(Pair<String, String> pair : pairs) {
			try {
				graph.addEdge(pair.getArgLeft(), pair.getArgRight());
				
			} catch (EdgeAlreadyExistsException e) {
				
			}
		}
	}

}
