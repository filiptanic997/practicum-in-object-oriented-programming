package undoManagement;

import java.util.*;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import graph.*;
import myUtil.*;
import javafx.scene.paint.*;

public class RemoveNodes extends AbstractUndoableEdit {

	private Graph graph;
	private HashMap<String, Quad<Double, Double, Double, String>> removedNodes;
	private HashSet<Pair<String, String>> removedEdges;
	private HashMap<String, Color> oldColors;
	private double positionX;
	private double positionY;
	private double radius;

	@SuppressWarnings("unchecked")
	public RemoveNodes(Graph graph, ArrayList<Node> selection) {
		this.graph = graph;
		this.removedNodes = new HashMap();
		this.removedEdges = new HashSet();
		this.oldColors = new HashMap();
		for (Node n1 : selection) {
			removedNodes.put(n1.getID(), new Quad(n1.getCenterX(), n1.getCenterY(),
													n1.getRadius(), n1.getTextLabel().getText()));
			
			oldColors.put(n1.getID(), (Color)n1.getFill());

			for (Node n2 : n1.getNeighbors()) {
				String node1ID = n1.getID();
				String node2ID = n2.getID();

				if (node1ID.compareTo(node2ID) > 0) {
					String temp = node1ID;
					node1ID = node2ID;
					node2ID = temp;
				}

				Pair<String, String> pair = new Pair(node1ID, node2ID);
				if (!removedEdges.contains(pair))
					removedEdges.add(pair);
			}
		}
		redo();
	}

	@Override
	public void undo() throws CannotUndoException {
		for (String nodeID : removedNodes.keySet()) {
			Quad<Double, Double, Double, String> q = removedNodes.get(nodeID);
			try {
				graph.addNode(new Node(nodeID,
						(double)q.get0(), (double)q.get1(), (double)q.get2(), q.get3(),
						oldColors.get(nodeID)));
			} catch (NodeAlreadyExistsException e) {
				// this will never happen
			}
		}
		for (Pair<String, String> pair : removedEdges) {
			try {
				graph.addEdge(pair.getArgLeft(), pair.getArgRight());
			} catch (EdgeAlreadyExistsException e) {
				// this will never happen
			}
		}
	}

	@Override
	public void redo() throws CannotRedoException {
		for (String nodeID : removedNodes.keySet()) {
			Node n = graph.getNode(nodeID);
			graph.removeNode(n);
		}
	}
}
