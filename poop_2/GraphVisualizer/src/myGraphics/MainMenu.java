package myGraphics;

import graph.*;
import graph.loader.*;
import distributed.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.embed.swing.SwingFXUtils;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javafx.stage.*;
import javafx.scene.control.Alert.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.Pane;

public class MainMenu extends javafx.scene.control.MenuBar {

	private Stage stage;
	private Menu file = new Menu("File");
	private MenuItem open = new MenuItem("Open...");
	private MenuItem expo = new MenuItem("Export");
	private MenuItem shot = new MenuItem("Snapshot");
	private MenuItem exit = new MenuItem("Exit");
	private Menu edit = new Menu("Edit");
	private MenuItem undo = new MenuItem("Undo");
	private MenuItem redo = new MenuItem("Redo");
	private Menu distribute = new Menu("Distribute");
	private MenuItem startServer = new MenuItem("Start Server");
	private MenuItem stopServer = new MenuItem("Stop Server");
	
	private GraphLoader loader;
	private GraphLoader customExporter = new CustomLoader();
	private graph.Graph loadTo;// set from the main class

	public MainMenu(Stage stage, graph.Graph graph) {
		this.stage = stage;
		loadTo = graph;
		getMenus().addAll(file, edit, distribute);
		file.getItems().addAll(open, expo, shot, exit);
		edit.getItems().addAll(undo, redo);
		distribute.getItems().addAll(startServer, stopServer);

		open.setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open Resource File");
			fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"),
					new FileChooser.ExtensionFilter("GML files (*.gml)", "*.gml"),
					new FileChooser.ExtensionFilter("Custom files (*.txt)", "*.txt"));
			File file = fileChooser.showOpenDialog(stage);

			if (file != null) {
				graph.clear();

				String fileName = file.getName();

				Pattern format = Pattern.compile("^.*\\.gml$");
				Matcher matcher = format.matcher(fileName);
				if (matcher.find()) {
					loader = new GMLLoader();

					loader.loadGraph(graph, file);
					
					Pane graphPane = graph.getRootPane();
					double xRange = graphPane.getWidth() * 0.9;
					double yRange = graphPane.getHeight() * 0.9;
					double originX = graphPane.getWidth() * 0.05;
					double originY = graphPane.getHeight() * 0.05;

					for (Node n : graph.getNodes()) {
						double randX = originX + Math.random() * xRange;
						double randY = originY + Math.random() * yRange;
						
						n.setPosition(randX, randY);
					}
				}

				format = Pattern.compile("^.*\\.csv$");
				matcher = format.matcher(fileName);
				if (matcher.find()) {
					loader = new CSVLoader();

					loader.loadGraph(graph, file);
					
					Pane graphPane = graph.getRootPane();
					double xRange = graphPane.getWidth() * 0.9;
					double yRange = graphPane.getHeight() * 0.9;
					double originX = graphPane.getWidth() * 0.05;
					double originY = graphPane.getHeight() * 0.05;

					for (Node n : graph.getNodes()) {
						double randX = originX + Math.random() * xRange;
						double randY = originY + Math.random() * yRange;
						
						n.setPosition(randX, randY);
					}
				}

				format = Pattern.compile("^.*\\.txt$");
				matcher = format.matcher(fileName);
				if (matcher.find()) {
					loader = new CustomLoader();

					loader.loadGraph(graph, file);
				}

			} else {

				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Warning Dialog");
				alert.setContentText("Wrong file format!");
				alert.initOwner(stage);

				alert.showAndWait();

			}		

		});
		
		expo.setOnAction(event -> {

			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Custom files (*.txt)", "*.txt"));
			File file = fileChooser.showSaveDialog(stage);
			
			if(file != null) {
				customExporter.writeGraph(graph, file);
			}
			else {
				Dialog<Boolean> dialog = new Dialog();
				dialog.setTitle("Error");
				dialog.setContentText("Failed!");
				dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
				dialog.showAndWait();
			}
		
		});

		shot.setOnAction(event -> {

			WritableImage snapshot = loadTo.getRootPane().snapshot(null, null);
			File file = new File("snapshot.png");
			try {
				ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", file);
			} catch (IOException e) {
				System.out.println("Error in saving snapshot");
			}

		});

		undo.setOnAction(event -> {
			MainWindow.mainWindow.undoManager.undo();
		});

		redo.setOnAction(event -> {
			MainWindow.mainWindow.undoManager.redo();
		});
		
		exit.setOnAction(event -> {
			stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
		});
		
		
		startServer.setOnAction(event -> {
			if(MainWindow.mainWindow.algoServer != null)
				return;
			
			MainWindow.mainWindow.algoServer = new AlgoServer(graph);
		});
		
		stopServer.setOnAction(event -> {
			if(MainWindow.mainWindow.algoServer == null)
				return;
			
			MainWindow.mainWindow.algoServer.stop();
			MainWindow.mainWindow.algoServer = null;
		});

	}

}
