package myGraphics;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import graph.*;
import graph.Node;
import graph.loader.CustomLoader;
import javafx.application.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.*;
import javafx.scene.layout.*;
import javafx.scene.input.*;
import javafx.beans.value.*;
import javafx.geometry.Point2D;
import javax.swing.undo.UndoManager;
import undoManagement.*;
import algorithm.*;
import distributed.*;

public class MainWindow extends Application {

	private static double currentScale = 1.0;
	Pane nodes = new Pane();
	Pane edges = new Pane();
	Pane nodeLabels = new Pane();
	Pane edgeLabels = new Pane();
	Boolean toAddNode = false;
	Boolean showNodeLabels = true;
	Boolean showEdgeLabels = false;
	Boolean movingNodes = false;
	UndoManager undoManager = new UndoManager();
	Point2D dragOrigin = null;
	Algorithm algorithm = null;
	AlgoServer algoServer = null;
	volatile Boolean isServerRunning = false;
	AnimatedZoomOperator zoomOperator = new AnimatedZoomOperator();

	private Graph g = new Graph(nodes, edges, nodeLabels, edgeLabels);

	public static MainWindow mainWindow;

	public MainWindow() {
		mainWindow = this;
	}

	public static double getCurrentScale() {
		return currentScale;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Button addNode = new Button("Add Node");
		TextField nodeID = new TextField();
		Button renameNode = new Button("Rename Node");
		Button removeNode = new Button("Remove Node");
		Button addEdge = new Button("Add Edge");
		Button removeEdge = new Button("Remove Edge");
		Button toggleNodeLabels = new Button("Toggle Node Labels");
		Button toggleEdgeLabels = new Button("Toggle Edge Labels");
		Button resizeNode = new Button("Resize Node");
		TextField nodeSize = new TextField();
		Button recolorNode = new Button("Recolor Node");
		ColorPicker colorSelect = new ColorPicker();
		Button shortestPath = new Button("Shortest Path");
		Button applyFormat = new Button("Apply");
		Button startStop = new Button("Start/Stop");
		Button applyParams = new Button("Apply");

		nodeLabels.setMouseTransparent(true);
		edgeLabels.setMouseTransparent(true);
		MainMenu mainMenu = new MainMenu(primaryStage, g);

		algorithm = new Expansion(g, 1.05);

		BorderPane rootPane = new BorderPane();

		VBox sideBar = new VBox();
		sideBar.setPrefWidth(200.);
		sideBar.prefHeightProperty().bind(sideBar.heightProperty());
		sideBar.setBackground(new Background(new BackgroundFill(Color.BLANCHEDALMOND, null, null)));

		GridPane addNodePane = new GridPane();
		addNodePane.prefWidthProperty().bind(sideBar.widthProperty());
		addNodePane.prefHeightProperty().bind(sideBar.heightProperty().divide(12.));
		addNode.prefWidthProperty().bind(addNodePane.widthProperty());
		nodeID.prefWidthProperty().bind(addNodePane.widthProperty());
		addNodePane.add(addNode, 0, 0);
		addNodePane.add(nodeID, 1, 0);

		ToggleGroup selectFormat = new ToggleGroup();// format pane
		RadioButton selectColorFormat = new RadioButton("Color");
		selectColorFormat.setToggleGroup(selectFormat);
		selectColorFormat.setSelected(true);
		RadioButton selectSizeFormat = new RadioButton("Node Size");
		selectSizeFormat.setToggleGroup(selectFormat);
		RadioButton selectSizeLabelFormat = new RadioButton("Label Size");
		selectSizeLabelFormat.setToggleGroup(selectFormat);

		GridPane colorFormating = new GridPane();
		ColorPicker startColor = new ColorPicker();
		startColor.prefWidthProperty().bind(colorFormating.widthProperty());
		startColor.prefHeightProperty().bind(colorFormating.heightProperty().divide(2));
		ColorPicker endColor = new ColorPicker();
		endColor.prefWidthProperty().bind(colorFormating.widthProperty());
		endColor.prefHeightProperty().bind(colorFormating.heightProperty().divide(2));
		colorFormating.add(startColor, 0, 0);
		colorFormating.add(endColor, 0, 1);

		GridPane nodeSizeFormating = new GridPane();
		TextField startNodeSize = new TextField();
		startNodeSize.prefWidthProperty().bind(nodeSizeFormating.widthProperty());
		startNodeSize.prefHeightProperty().bind(nodeSizeFormating.heightProperty().divide(2));
		TextField endNodeSize = new TextField();
		endNodeSize.prefWidthProperty().bind(nodeSizeFormating.widthProperty());
		endNodeSize.prefHeightProperty().bind(nodeSizeFormating.heightProperty().divide(2));
		nodeSizeFormating.add(startNodeSize, 0, 0);
		nodeSizeFormating.add(endNodeSize, 0, 1);

		GridPane labelSizeFormating = new GridPane();
		TextField startLabelSize = new TextField();
		startLabelSize.prefWidthProperty().bind(labelSizeFormating.widthProperty());
		startLabelSize.prefHeightProperty().bind(labelSizeFormating.heightProperty().divide(2));
		TextField endLabelSize = new TextField();
		endLabelSize.prefWidthProperty().bind(labelSizeFormating.widthProperty());
		endLabelSize.prefHeightProperty().bind(labelSizeFormating.heightProperty().divide(2));
		labelSizeFormating.add(startLabelSize, 0, 0);
		labelSizeFormating.add(endLabelSize, 0, 1);

		StackPane formatPane = new StackPane();
		formatPane.prefWidthProperty().bind(sideBar.widthProperty());
		formatPane.prefHeightProperty().bind(sideBar.heightProperty().divide(8.));

		formatPane.getChildren().addAll(labelSizeFormating, nodeSizeFormating, colorFormating);
		applyFormat.prefWidthProperty().bind(sideBar.widthProperty());

		StackPane algorithmPane = new StackPane();

		ToggleGroup selectAlgo = new ToggleGroup();// algorithm pane
		RadioButton expansion = new RadioButton("Expansion");
		expansion.setToggleGroup(selectAlgo);
		expansion.setSelected(true);
		RadioButton contraction = new RadioButton("Contraction");
		contraction.setToggleGroup(selectAlgo);
		RadioButton forceAtlas2 = new RadioButton("ForceAtlas 2");
		forceAtlas2.setToggleGroup(selectAlgo);

		GridPane expansionContractionPane = new GridPane();
		Label scalingFactorLabel = new Label("Scale");
		scalingFactorLabel.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.33));
		scalingFactorLabel.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		TextField scalingFactor = new TextField("1.05");
		scalingFactor.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.67));
		scalingFactor.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		expansionContractionPane.add(scalingFactorLabel, 0, 0);
		expansionContractionPane.add(scalingFactor, 1, 0);
		expansionContractionPane.setBackground(new Background(new BackgroundFill(Color.BLANCHEDALMOND, null, null)));

		GridPane fa2Pane = new GridPane();
		Label gravity = new Label("Gravity");
		gravity.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.33));
		gravity.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		TextField gravityField = new TextField("1.0");
		gravityField.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.67));
		gravityField.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		Label scale = new Label("Scale");
		scale.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.33));
		scale.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		TextField scaleField = new TextField("1.05");
		scaleField.prefWidthProperty().bind(algorithmPane.widthProperty().multiply(0.67));
		scaleField.prefHeightProperty().bind(algorithmPane.heightProperty().multiply(0.5));
		fa2Pane.add(gravity, 0, 0);
		fa2Pane.add(gravityField, 1, 0);
		fa2Pane.add(scale, 0, 1);
		fa2Pane.add(scaleField, 1, 1);
		fa2Pane.setBackground(new Background(new BackgroundFill(Color.BLANCHEDALMOND, null, null)));

		algorithmPane.getChildren().addAll(fa2Pane, expansionContractionPane);

		GridPane algoControl = new GridPane();
		applyParams.prefWidthProperty().bind(sideBar.widthProperty().multiply(0.33));
		algoControl.add(applyParams, 0, 0);
		startStop.prefWidthProperty().bind(sideBar.widthProperty().multiply(0.67));
		algoControl.add(startStop, 1, 0);

		sideBar.getChildren().addAll(addNodePane, renameNode, removeNode, addEdge, removeEdge, toggleNodeLabels,
				toggleEdgeLabels, resizeNode, nodeSize, recolorNode, colorSelect, shortestPath, selectColorFormat,
				selectSizeFormat, selectSizeLabelFormat, formatPane, applyFormat, expansion, contraction, forceAtlas2,
				algorithmPane, algoControl);

		Pane graphPane = new Pane(edges, nodes, edgeLabels, nodeLabels);
		rootPane.setCenter(graphPane);
		rootPane.setLeft(sideBar);
		rootPane.setTop(mainMenu);

		Scene scene = new Scene(rootPane, 960, 640);
		mainMenu.prefWidthProperty().bind(scene.widthProperty());
		graphPane.prefWidthProperty().bind(scene.widthProperty());
		graphPane.prefHeightProperty().bind(scene.heightProperty());
		scene.setFill(Color.LIGHTCYAN);

		primaryStage.setTitle("Sample Application");
		primaryStage.setScene(scene);
		primaryStage.show();

		addNode.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			toAddNode = true;
		});

		nodeID.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[A-Z0-9]*"))
					nodeID.setText(oldValue);
				else
					nodeID.setText(newValue);
			}
		});

		renameNode.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if (graph.Node.getSelected().size() == 1 && !nodeID.getText().equals("")) {
				if (g.getNode(nodeID.getText()) == null) {
					undoManager.addEdit(new RenameNode(g, graph.Node.getSelected().get(0), nodeID.getText()));
				} else {
					Dialog<Boolean> dialog = new Dialog();
					dialog.setTitle("Error");
					dialog.setContentText("Error! Node with the given id alreay exists");
					dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
					dialog.showAndWait();
				}
			}
		});

		removeNode.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			undoManager.addEdit(new RemoveNodes(g, graph.Node.getSelected()));
			graph.Node.unselectAll();
		});

		addEdge.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			undoManager.addEdit(new AddEdges(g, graph.Node.getSelected()));
		});

		removeEdge.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			undoManager.addEdit(new RemoveEdges(g, graph.Node.getSelected()));
		});

		toggleNodeLabels.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
			for (javafx.scene.Node node : nodes.getChildren()) {
				if (showNodeLabels)
					((graph.Node) node).setLabelVisible(false);
				else
					((graph.Node) node).setLabelVisible(true);
			}
			showNodeLabels = showNodeLabels ? false : true;
		});

		toggleEdgeLabels.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
			for (javafx.scene.Node node : edges.getChildren()) {
				if (showEdgeLabels)
					((graph.Edge) node).setLabelVisible(false);
				else
					((graph.Edge) node).setLabelVisible(true);
			}
			showEdgeLabels = showEdgeLabels ? false : true;
		});

		resizeNode.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
			double size = Double.parseDouble(nodeSize.getText());
			undoManager.addEdit(new ResizeNodes(g, graph.Node.getSelected(), size));
			nodeSize.setText("0.0");
		});

		nodeSize.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*(\\.\\d*)?"))
					nodeSize.setText(oldValue);
				else
					nodeSize.setText(newValue);
			}
		});

		recolorNode.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if (graph.Node.getSelected().size() > 0) {
				undoManager.addEdit(new RecolorNodes(g, graph.Node.getSelected(), colorSelect.getValue()));

			}
		});

		shortestPath.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if (graph.Node.getSelected().size() == 2) {
				for (graph.Node node : g.findShortestPathBFS(graph.Node.getSelected().get(0),
						graph.Node.getSelected().get(1))) {

					graph.Node.select(node);
				}
			}
		});

		selectColorFormat.selectedProperty().addListener(listener -> {
			formatPane.getChildren().remove(colorFormating);
			formatPane.getChildren().add(colorFormating);
		});

		selectSizeFormat.selectedProperty().addListener(listener -> {
			formatPane.getChildren().remove(nodeSizeFormating);
			formatPane.getChildren().add(nodeSizeFormating);
		});

		selectSizeLabelFormat.selectedProperty().addListener(listener -> {
			formatPane.getChildren().remove(labelSizeFormating);
			formatPane.getChildren().add(labelSizeFormating);
		});

		startNodeSize.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[0-9]*"))
					startNodeSize.setText(oldValue);
				else
					startNodeSize.setText(newValue);
			}
		});

		endNodeSize.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[0-9]*"))
					endNodeSize.setText(oldValue);
				else
					endNodeSize.setText(newValue);
			}
		});

		startLabelSize.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[0-9]*"))
					startLabelSize.setText(oldValue);
				else
					startLabelSize.setText(newValue);
			}
		});

		endLabelSize.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("[0-9]*"))
					endLabelSize.setText(oldValue);
				else
					endLabelSize.setText(newValue);
			}
		});

		applyFormat.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if (selectColorFormat.isSelected()) {
				g.formatNodeColor(startColor.getValue(), endColor.getValue());
			} else if (selectSizeFormat.isSelected()) {
				if (!startNodeSize.getText().equals("") && !endNodeSize.getText().equals("")
						&& Integer.parseInt(startNodeSize.getText()) <= Integer.parseInt(endNodeSize.getText())) {

					g.formatNodeSize(Integer.parseInt(startNodeSize.getText()),
							Integer.parseInt(endNodeSize.getText()));
				} else {
					// dialog?
				}
			} else if (selectSizeLabelFormat.isSelected()) {
				if (!startLabelSize.getText().equals("") && !endLabelSize.getText().equals("")
						&& Integer.parseInt(startLabelSize.getText()) <= Integer.parseInt(endLabelSize.getText())) {

					g.formatLabelSize(Integer.parseInt(startLabelSize.getText()),
							Integer.parseInt(endLabelSize.getText()));
				} else {
					// dialog?
				}
			}
		});

		expansion.setOnAction(event -> {
			algorithmPane.getChildren().remove(expansionContractionPane);
			algorithmPane.getChildren().add(expansionContractionPane);
			if (algoServer != null) {
				algoServer.setAlgorithm(AlgoServer.Algorithm.Expansion);
			} else {
				if (algorithm != null)
					algorithm.stopAction();
				algorithm = new Expansion(g, Double.parseDouble(scalingFactor.getText()));
				algorithm.start();
			}
		});

		contraction.setOnAction(event -> {
			algorithmPane.getChildren().remove(expansionContractionPane);
			algorithmPane.getChildren().add(expansionContractionPane);
			if (algorithm != null)
				algorithm.stopAction();
			algorithm = new Contraction(g, Double.parseDouble(scalingFactor.getText()));
			algorithm.start();
		});

		forceAtlas2.setOnAction(event -> {
			algorithmPane.getChildren().remove(fa2Pane);
			algorithmPane.getChildren().add(fa2Pane);
			if (algorithm != null)
				algorithm.stopAction();
			algorithm = new ForceAtlas2(g, Double.parseDouble(gravityField.getText()),
					Double.parseDouble(scaleField.getText()));
			algorithm.start();
		});

		scalingFactor.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*(\\.\\d*)?"))
					scalingFactor.setText(oldValue);
				else
					scalingFactor.setText(newValue);
			}
		});

		gravityField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*(\\.\\d*)?"))
					gravityField.setText(oldValue);
				else
					gravityField.setText(newValue);
			}
		});

		scaleField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*(\\.\\d*)?"))
					scaleField.setText(oldValue);
				else
					scaleField.setText(newValue);
			}
		});

		startStop.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if(algoServer != null) {
				algoServer.start();
			}
			else {
				if (algorithm.getPaused())
					algorithm.resumeAction();
				else
					algorithm.pauseAction();
			}
		});

		applyParams.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
			if (algorithm instanceof ForceAtlas2) {
				algorithm.setParams(Double.parseDouble(gravityField.getText()),
						Double.parseDouble(scaleField.getText()));
			} else
				algorithm.setParams(0.0, Double.parseDouble(scalingFactor.getText()));
		});

		graphPane.addEventHandler(MouseEvent.DRAG_DETECTED, event -> {
			if (event.getTarget() != graphPane) {
				dragOrigin = new Point2D(event.getSceneX() - sideBar.getWidth(),
						event.getSceneY() - mainMenu.getHeight());
				movingNodes = true;
			}
		});

		graphPane.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
			if (movingNodes == true) {// multithreading?
				movingNodes = false;
				undoManager.addEdit(new MoveNodes(g, graph.Node.getSelected(), dragOrigin,
						new Point2D(event.getSceneX() - sideBar.getWidth(), event.getSceneY() - mainMenu.getHeight())));
			}
		});

		graphPane.setOnScroll(event -> {
			double zoomFactor = 1.5;
			if (event.getDeltaY() <= 0) {
				// zoom out
				zoomFactor = 1 / zoomFactor;
			}
			zoomOperator.zoom(graphPane, zoomFactor, event.getSceneX(), event.getSceneY());
		});

		scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
			if (event.isControlDown()) {
				if (event.getCode() == KeyCode.A) {
					for (javafx.scene.Node n : nodes.getChildren()) {
						graph.Node.select((graph.Node) n);
					}
				} else if (event.getCode() == KeyCode.Z) {
					try {
						undoManager.undo();
					} catch (Exception e) {
					}
				} else if (event.getCode() == KeyCode.Y) {
					try {
						undoManager.redo();
					} catch (Exception e) {
					}
				} else if (event.getCode() == KeyCode.S && graph.Node.getSelected().size() == 2) {
					ArrayList<Node> path = g.findShortestPathBFS(graph.Node.getSelected().get(0),
							graph.Node.getSelected().get(1));

					path.add(0, graph.Node.getSelected().get(0));
					path.add(path.size(), graph.Node.getSelected().get(1));

					new Salesman(nodes, path).start();
				}
			}
		});

		scene.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
			graph.Node.unselectAll();
			if (toAddNode && !nodeID.getText().equals("")) {
				undoManager.addEdit(new AddNode(g, nodeID.getText(), event.getSceneX() - sideBar.getWidth(),
						event.getSceneY() - mainMenu.getHeight(), 30.));
			}

			toAddNode = false;
		});

		primaryStage.setOnCloseRequest(event -> {
			Alert dialog = new Alert(AlertType.INFORMATION, "All unsaved data will be lost, do you wish to save?",
					ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
			dialog.setTitle("Exit?");

			Optional<ButtonType> clicked = dialog.showAndWait();

			if (clicked.get() == ButtonType.CANCEL) {
				event.consume();
				return;
			} else if (clicked.get() == ButtonType.YES) {
				FileChooser chooser = new FileChooser();
				chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Custom files (*.txt)", "*.txt"));
				File saveTo = chooser.showSaveDialog(primaryStage);

				if (saveTo != null) {
					new CustomLoader().writeGraph(g, saveTo);
				} else {
					dialog.setTitle("Error");
					dialog.setContentText("Failed!");
					dialog.getDialogPane().getButtonTypes().add(new ButtonType("Got it!", ButtonData.CANCEL_CLOSE));
					dialog.showAndWait();
				}
			}

			if (algorithm != null)
				algorithm.stopAction();
			if (algoServer != null)
				algoServer.stop();

			Platform.exit();
		});

	}

}
