package distributed;

import java.io.*;
import java.net.*;
import java.util.*;
import graph.*;
import javafx.application.Platform;

class Package implements Serializable {
	AlgoServer.Algorithm algorithm;
	HashMap<String, SimplifiedNode> graph;
}

public class AlgoServer implements Runnable {

	public enum Algorithm {
		Expansion, Contraction, ForceAtlas2, SalesMan
	};

	private ServerSocket serverSocket;
	private Socket clientSocket;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private Thread t;

	private Graph graph;
	private volatile HashMap<String, SimplifiedNode> simplifiedGraph;
	private volatile Algorithm algorithm;

	public AlgoServer(Graph graph) {
		try {
			System.out.println("Server Started");
			serverSocket = new ServerSocket(10);
			t = new Thread(this);
			this.graph = graph;

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;

	}

	public void start() {
		t.start();
	}

	public void stop() {
		t.interrupt();
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				clientSocket = serverSocket.accept();
				// System.out.println("Connected to client " + clientSocket.getInetAddress());
				outStream = new ObjectOutputStream(clientSocket.getOutputStream());

				// outStream.writeObject(algorithm);

				simplifiedGraph = new HashMap();

				for (Node node : graph.getNodes()) {
					SimplifiedNode n = new SimplifiedNode(node.getID(), node.getCenterX(), node.getCenterY());
					simplifiedGraph.put(node.getID(), n);
				}

				outStream.writeObject(simplifiedGraph);// send the original data to client
				outStream.flush();

				inStream = new ObjectInputStream(clientSocket.getInputStream());
				
				while (!Thread.interrupted()) {// receive updated node positions

					while (true) {
						try {
							Thread.sleep(7);
							simplifiedGraph = (HashMap<String, SimplifiedNode>) inStream.readObject();
							break;
						} catch (StreamCorruptedException e) {

						} catch (EOFException e) {

						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							for (SimplifiedNode n : simplifiedGraph.values()) {
								Node node = graph.getNode(n.nodeID);
								node.setPosition(n.x, n.y);
								System.out.println(n.x + " " + n.y);
							}
						}
					});

				}

				clientSocket.close();
				System.out.println("Closed connection...");
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
